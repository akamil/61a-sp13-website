
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
          "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"> 
  <head>
    <meta name="description" content ="CS61A: Structure and Interpretation of Computer Programs" /> 
    <meta name="keywords" content ="CS61A, Computer Science, CS, 61A, Programming, Berkeley, EECS" /> 
    <meta name="author" content ="Tom Magrino, Jon Kotker, Eric Kim, Steven Tang, Joy Jeng, Stephen Martinis, Allen Nguyen, Albert Wu" /> 
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 
    <style type="text/css">@import url("https://inst.eecs.berkeley.edu/~cs61a/su12/lab/lab_style.css");</style> 

    <title>CS 61A Fall 2012: Lab 2</title> 

    <?php
    /* So all of the PHP in this file is to allow for this nice little trick to 
     * help us avoid having two versions of the questions lying around in the 
     * repository, which often leads to the two versions going out of sync which 
     * leads to annoyance for students.
     *
     * The idea's pretty simple for the PHP part, just simply have two dates: 
     *
     *    1. The current date
     *    2. The date the solutions should be released
     *
     * Using these, we now wrap our solutions in a simple PHP if statement that 
     * checks if the date is past the release date and only includes the code on 
     * the page displayed (what the server gives back to the browser) if the 
     * solutions are supposed to be released.
     *
     * We also use some PHP to create unique IDs for each of the show/hide 
     * buttons and solution divs, which are then used in the PHP generated 
     * jQuery code that we use to create the nice toggling effect.
     *
     * I apologize if the PHP/jQuery is really offensively bad, this is 
     * literally the most I've written of either for a single project so far.
     * Comments/suggestions are most welcome!
     *
     * - Tom Magrino (tmagrino@berkeley.edu)
     */
    $BERKELEY_TZ = new DateTimeZone("America/Los_Angeles");
    $RELEASE_DATE = new DateTime("02/08/2013", $BERKELEY_TZ);
    $CUR_DATE = new DateTime("now", $BERKELEY_TZ);
    $q_num = 0; // Used to make unique ids for all solutions and buttons
    ?>
  </head> 
  
  <body style="font-family: Georgia,serif;"> 

<h1>CS61A Lab 2:</h1>
<h2>Functions - Higher Order, Lambdas</h2>
<h3>Week 3, Spring 2013</h3>

<h3 class="section_title">Exercise 1: Higher Order Functions</h3>


<p>For every line that is marked with a "# ?" symbol, try to determine 
what Python would print in the interactive interpreter. Then check to see if 
you got the answer right.</p>

<pre class="codemargin">
>>> def square(x):
	return x*x
	
>>> def neg(f, x):
	return -f(x)
# Q1
>>> neg(square, 4)
_______________
>>> def first(x):
...	x += 8
...	def second(y):
...		print('second')
...		return x + y
...	print('first')
...	return second
...
# Q2
>>> f = first(15)
_______________
# Q3
>>> f(16)
_______________

>>> def foo(x):
...	def bar(y):
...		return x + y
...	return bar

>>> boom = foo(23)
# Q4
>>> boom(42)
_______________
# Q5
>>> foo(6)(7)
_______________

>>> func = boom
# Q6
>>> func is boom
_______________
>>> func = foo(23)
# Q7
>>> func is boom
_______________
>>> def Troy():
...  abed = 0
...  while abed < 10:
...    britta = lambda: abed
...    abed += 1
...  abed = 20
...  return britta
...
>>> jeff = Troy()
>>> shirley = lambda : jeff
>>> pierce = shirley()
# Q8
>>> pierce()
________________
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p>
	  <pre class="codemargin">
We recommend you try typing these statements into the interpreter.

1) -16
2) first
3) second
   39
4) 65
5) 13
6) True
7) False
8) 20
	</pre>
	  </p>
    </div>
    <?php } ?>

<h3 class="section_title">Exercise 2: Returning Functions</h3>

<p> In your Hog project, you will have to work with strategies and functions that
make strategies. Let's start with defining what a strategy is: a function that
takes two arguments (your score and the opponent's score) and returns the number
of dice to toll. </p>

<p> Make a strategy function that always rolls <code>5</code> dice. This is the strategy
that is the computer's default strategy. Call this <span class="code">default_strategy</span>.
Now, let's define a function that will return this <span class="code">default_strategy</span>
when it's evaluated. Call this function <span class="code"> make_default_strategy</span>. </p>

<p> Now try implementing a strategy maker called <span class="code"> make_weird_strategy</span>
 that will take one argument called <span class="code">num_rolls</span>. The strategy it returns
 will return the higher of <span class="code"> num_rolls</span> or the total number of points 
scored in the game thus far divided by <code>20</code>, throwing away any remainder.</p>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
     <p>
	  <pre class="codemargin">
def default_strategy(score, op_score):
    return 5

def make_default_strategy():
    def default_strategy(score, op_score):
        return 5
    return default_strategy

def make_weird_strategy(num_rolls):
    def weird_strategy(score, op_score):
        return max(num_rolls, (score+op_score)//20)
    return weird_strategy
	</pre>
	  </p>
    </div>
    <?php } ?>

<h3 class="section_title">Exercise 3: Lambdas</h3>

<p>Now, we will visit the idea of <i>lambda</i> functions. These are functions that are one line
functions, and that line just specifies the return value. The format of a lambda function is as follows:</p>

<pre class="codemargin">
lambda [parameters]: [return value]
</pre>

<p> Another bit of syntax that we introduce in this exercise is the conditional expression. It has the following
format:</p>

<pre class="codemargin">
[true-value] if [conditional] else [false-value]
</pre>

<p> It is almost equivalent to the if statement that you have already seen. If the conditional evaluates to True
then the conditional expression evaluates to [true-value]. If it's false, then it evaluates to [false-value].</p>

<p> Knowing this, go back to Exercise 1 and make sure you understand the lambda question. Now, fill in the
blanks as to what Python would do here:</p>

<pre class="codemargin">
>>> doctor = lambda : "who"
# Q1
>>> doctor()
_______________
>>> ninth = lambda x: "Fantastic!" if x == 9 else tenth
>>> tenth = lambda y: "Allons-y!" if y == 10 else eleventh
>>> eleventh = lambda z: "Geronimo!" if z == 11 else ninth
# Q2
>>> ninth(9)
_______________
# Q3
>>> ninth(2)(10)
_______________
# Q4
>>> tenth(10)
_______________
# Q5
>>> tenth(12) is eleventh
_______________
# Q6
>>> eleventh(10)(11)(9)(11)
_______________
</pre>

<p> As a final challenge, try to rewrite the previous exercise using solely lambdas.
</p>
<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p>
	  <pre class="codemargin">
          1) "who"
          2) "Fantastic!"
          3) "Allons-y!"
          4) "Allons-y!"
          5) True
          6) "Geronimo!"

          >>> default_strategy = lambda score, op_score : 5
          >>> make_default_strategy = lambda : lambda score, op_score : 5
          >>> make_weird_strategy = lambda num_rolls : lambda score, op_score : max(num_rolls, (score+op_score)//20)
	  </pre>
	  </p>
    </div>
    <?php } ?>

<h3 class="section_title">Exercise 4: I Heard You Liked Functions So I Put Functions In Your Functions </h3>

<p>Define a function <span class="code">cycle</span> which takes in three
arguments and returns another function. The returned fuction should take in an
integer argument <span class="code">n</span> such that if the argument is <span class="code">0</span>, 
a function is returned that takes in an argument <span class="code">x</span> and 
simply returns that argument x. If the argument is <span class="code">1</span>,
the first function given to cycle should be applied to <span class="code">x</span>.
If it's <span class="code">2</span>, the first is applied to <span class="code">x</span>
and then the second argument is applied. Likewise, if it's <span class="code">3</span>,
it's the first, then second, then third. For <span class="code">4</span>, you do
the first, second, third, then the first again. For higher numbers you repeat this
cycle.

</p>
<p>Hint: most of the work goes inside the most nested function.</p>
<pre class="codemargin">
def cycle(f1, f2, f3):
    """ Returns a function that is itself a higher order function 
    >>> add1 = lambda x: x+1 
    >>> times2 = lambda x: 2*x 
    >>> add3 = lambda x: x+3 
    >>> my_cycle = cycle(add1, times2, add3)
    >>> identity = my_cycle(0)
    >>> identity(5)
    5
    >>> add_one_then_double = my_cycle(2)
    >>> add_one_then_double(1)
    4
    >>> do_all_functions = my_cycle(3)
    >>> do_all_functions(2)
    9
    >>> do_more_than_a_cycle = my_cycle(4)
    >>> do_more_than_a_cycle(2)
    10
    >>> do_two_cycles = my_cycle(6)
    >>> do_two_cycles(1)
    19
    """

    " *** YOUR CODE HERE *** "
</pre>

	<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
<pre><b>

def cycle(f1, f2, f3):
     def ret_fn(n):            
         def ret(x):      
             i = 0       
             while i < n:               
                 if i % 3 == 0:
                     x = f1(x) 
                 elif i % 3 == 1:
                     x = f2(x)
                 else:        
                     x = f3(x)
                 i += 1
             return x
         return ret
     return ret_fn     
</b></pre>
</div>
<?php } ?>


<p> Fin. <p>

    <?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script>
      <?php for ($i = 0; $i < $q_num; $i++) { ?>
    $("#toggleButton<?php echo $i; ?>").click(function () {
      $("#toggleText<?php echo $i; ?>").toggle();
    });
      <?php } ?>
    </script>
    <?php } ?>
  </body>
</html>
