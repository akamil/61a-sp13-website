class Bank:
    def __init__(self, name):
        self.name = name
        self.__balance = 0

    def deposit(self, amount):
        try:
            if amount < 0:
                print("Cannot deposit a negative amount.")
            else:
                self.__balance += amount
        except TypeError:
            print("Must deposit a numerical amount.")

    def withdraw(self, amount):
        try:
            if amount < 0:
                print("Cannot withdraw a negative amount.")
            else:
                self.__balance -= amount
        except TypeError:
            print("Must withdraw a numerical amount.")

    @property
    def balance(self):
        return self.__balance
