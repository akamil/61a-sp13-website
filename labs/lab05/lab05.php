<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
          "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <meta name="description" content ="CS61A: Structure and Interpretation of Computer Programs" />
    <meta name="keywords" content ="CS61A, Computer Science, CS, 61A, Programming, Berkeley, EECS" />
    <meta name="author" content ="Amir Kamil, Hamilton Nguyen, Joy Jeng, Keegan Mann, Stephen Martinis, Albert Wu, Julia Oh, Robert Huang, Mark Miyashita, Sharad Vikram, Soumya Basu, Richard Hwang" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style type="text/css">@import url("https://inst.eecs.berkeley.edu/~cs61a/su12/lab/lab_style.css");</style>

    <title>CS 61A Spring 2013: Lab 5</title>

    <?php
    /* So all of the PHP in this file is to allow for this nice little trick to
     * help us avoid having two versions of the questions lying around in the
     * repository, which often leads to the two versions going out of sync which
     * leads to annoyance for students.
     *
     * The idea's pretty simple for the PHP part, just simply have two dates:
     *
     *    1. The current date
     *    2. The date the solutions should be released
     *
     * Using these, we now wrap our solutions in a simple PHP if statement that
     * checks if the date is past the release date and only includes the code on
     * the page displayed (what the server gives back to the browser) if the
     * solutions are supposed to be released.
     *
     * We also use some PHP to create unique IDs for each of the show/hide
     * buttons and solution divs, which are then used in the PHP generated
     * jQuery code that we use to create the nice toggling effect.
     *
     * I apologize if the PHP/jQuery is really offensively bad, this is
     * literally the most I've written of either for a single project so far.
     * Comments/suggestions are most welcome!
     *
     * - Tom Magrino (tmagrino@berkeley.edu)
     */
    $BERKELEY_TZ = new DateTimeZone("America/Los_Angeles");
    $RELEASE_DATE = new DateTime("02/28/2013", $BERKELEY_TZ);
    $CUR_DATE = new DateTime("now", $BERKELEY_TZ);
    $q_num = 0; // Used to make unique ids for all solutions and buttons
    ?>
  </head>

  <body style="font-family: Georgia,serif;">

<h1>CS61A Lab 5: Tuples, Recursive Lists, and Strings</h1>
<h3>Week 6, 2013</h3>

<p>You can get the starter file for this lab by typing this command
into your terminal:</p>

<pre class='codemargin'>
cp ~cs61a/lib/lab/lab05/lab5.py .
</pre>

<p>Don't forget the dot at the end!</p>

<h3 class="section_title">Warm up: What would Python print?</h3>

<p>Predict what Python will display when you type the following into
the interpreter. Then try it to check your answers.</p>

<pre class='codemargin'>
&gt;&gt;&gt; x = (1, 2, 3)
&gt;&gt;&gt; x[0]     # Q1
______
&gt;&gt;&gt; x[3]     # Q2
______

&gt;&gt;&gt; x[-1]    # Q3
______
&gt;&gt;&gt; x[-3]    # Q4
______
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <pre class='codemargin'>
1) 1
2) IndexError
3) 3
4) 1
	  </pre>
    </div>
    <?php } ?>

<pre class='codemargin'>
&gt;&gt;&gt; x[:2]        # Q5
______
&gt;&gt;&gt; x[1:3]       # Q6
______
&gt;&gt;&gt; x[-2:3]      # Q7
______

&gt;&gt;&gt; x[::2]       # Q8
______
&gt;&gt;&gt; x[::-1]      # Q9
______
&gt;&gt;&gt; x[-1:0:-1]   # Q10
______
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <pre class='codemargin'>
5) (1, 2)
6) (2, 3)
7) (2, 3)
8) (1, 3)
9) (3, 2, 1)
10) (3, 2)
	  </pre>
    </div>
    <?php } ?>

<pre class='codemargin'>
&gt;&gt;&gt; y = (1,)
&gt;&gt;&gt; len(y)       # Q11
______
&gt;&gt;&gt; 1 in y       # Q12
______

&gt;&gt;&gt; y + (2, 3)   # Q13
______
&gt;&gt;&gt; (0,) + y     # Q14
______
&gt;&gt;&gt; y * 3        # Q15
______

&gt;&gt;&gt; z = ((1, 2), (3, 4, 5))
&gt;&gt;&gt; len(z)       # Q16
______
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <pre class='codemargin'>
11) 1
12) True
13) (1, 2, 3)
14) (0, 1)
15) (1, 1, 1)
16) 2
	  </pre>
    </div>
    <?php } ?>

<h3 class="section_title">Tuples</h3>

<p>1.) For each of the following, give the correct expression to get 7.
</p>

<pre class='codemargin'>
&gt;&gt;&gt; x = (1, 3, 5, 7)
&gt;&gt;&gt; x[-1]    # example
7

&gt;&gt;&gt; x = (1, 3, (5, 7), 9)
&gt;&gt;&gt; # YOUR EXPRESSION INVOLVING x HERE
7

&gt;&gt;&gt; x = ((7,),)
&gt;&gt;&gt; # YOUR EXPRESSION INVOLVING x HERE
7

&gt;&gt;&gt; x = (1, (2, (3, (4, (5, (6, (7,)))))))
&gt;&gt;&gt; # YOUR EXPRESSION INVOLVING x HERE
7
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <pre class='codemargin'>
1) x[2][1]
2) x[0][0]
3) x[1][1][1][1][1][1][0]
	  </pre>
    </div>
    <?php } ?>

<p>2.) Write a function <span class='code'>reverse</span> which takes a
tuple and returns the reverse. Write both a iterative and a recursive
version. You may use slicing notation, but don't use <span
class='code'>tup[::-1]</span>.

<pre class='codemargin'>
def reverse_iter(tup):
    """Returns the reverse of the given tuple.

    &gt;&gt;&gt; reverse_iter((1, 2, 3, 4))
    (4, 3, 2, 1)
    """
    "*** YOUR CODE HERE ***"

def reverse_recursive(tup):
    """Returns the reverse of the given tuple.

    &gt;&gt;&gt; reverse_revursive((1, 2, 3, 4))
    (4, 3, 2, 1)
    """
    "*** YOUR CODE HERE ***"
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <pre class='codemargin'>
def reverse_iter(tup):
    new, i = (), 0
    while i &lt; len(tup):
        new = (tup[i],) + new
        i += 1
    return new

def reverse_recursive(tup):
    if not tup:
        return ()
    return reverse_recursive(tup[1:]) + (tup[0],)
	  </pre>
    </div>
    <?php } ?>

<p>3.) Write a function <span class="code">merge</span> which takes
2 <i>sorted</i> tuples <span class="code">tup1</span> and
<span class='code'>tup2</span>, and returns a new tuple that contains
all the elements in the two tuples in sorted order. Write both a
recursive and an iterative version</p>

<pre class='codemargin'>
def merge_iter(tup1, tup2):
    """Merges two sorted tuples.

    &gt;&gt;&gt; merge_iter((1, 3, 5), (2, 4, 6))
    (1, 2, 3, 4, 5, 6)
    &gt;&gt;&gt; merge_iter((), (2, 4, 6))
    (2, 4, 6)
    &gt;&gt;&gt; merge_iter((1, 2, 3), ())
    (1, 2, 3)
    """
    "*** YOUR CODE HERE ***"

def merge_recursive(tup1, tup2):
    """Merges two sorted tuples.

    &gt;&gt;&gt; merge_recursive((1, 3, 5), (2, 4, 6))
    (1, 2, 3, 4, 5, 6)
    &gt;&gt;&gt; merge_recursive((), (2, 4, 6))
    (2, 4, 6)
    &gt;&gt;&gt; merge_recursive((1, 2, 3), ())
    (1, 2, 3)
    """
    "*** YOUR CODE HERE ***"
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <pre class='codemargin'>
def merge_iter(tup1, tup2):
    new = ()
    while tup1 and tup2:
        if tup1[0] &lt; tup2[0]:
            new += (tup1[0],)
            tup1 = tup1[1:]
        else:
            new += (tup2[0],)
            tup2 = tup2[1:]
    if tup1:
        return new + tup1
    else:
        return new + tup2

def merge_recursive(tup1, tup2):
    if not tup1 or not tup2:
        return tup1 + tup2
    elif tup1[0] &lt; tup2[0]:
        return (tup1[0],) + merge_recursive(tup1[1:], tup2)
    else:
        return (tup2[0],) + merge_recursive(tup1, tup2[1:])
	  </pre>
    </div>
    <?php } ?>

<p>4.) A tuple that contains one or more tuples as elements is called a
<i>deep</i> tuple. For example,
<span class='code'>(1, (2, 3), 4)</span> is a deep tuple.</p>

<p>Write a function <span class='code'>deep_len</span> that takes a
tuple and reports its deep length. See the doctests for the function's
behavior. You may write this function iteratively or recursively.</p>

<p><b>Hint</b>: you can check if something is a tuple by using the
built-in <span class='code'>type</span> function. For example,</p>

<pre class='codemargin'>
&gt;&gt;&gt; type(3) == tuple:
False
&gt;&gt;&gt; type((1, 2, 3)) == tuple:
True
</pre>


<pre class='codemargin'>
def deep_len(tup):
    """Returns the deep length of the tuple.

    &gt;&gt;&gt; deep_len((1, 2, 3))      # normal tuple
    3
    &gt;&gt;&gt; x = (1, (2, 3), 4)       # deep tuple
    &gt;&gt;&gt; deep_len(x)
    4
    &gt;&gt;&gt; y = ((1, (1, 1)), 1, (1, 1))  # deep tuple
    &gt;&gt;&gt; deep_len(y)
    6
    """
    "*** YOUR CODE HERE ***"
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <pre class='codemargin'>
def deep_len(tup):
    if not tup:
        return 0
    elif type(tup[0]) == tuple:
        return deep_len(tup[0]) + deep_len(tup[1:])
    else:
        return 1 + deep_len(tup[1:])
	  </pre>
    </div>
    <?php } ?>


<p>One last thing about tuples: they're <i>immutable</i> data
structures. This means that once they are created, they can't be
changed. For example, try this:</p>

<pre class='codemargin'>
&gt;&gt;&gt; x = (1, 2, 3)
&gt;&gt;&gt; x[0] = 4
</pre>

<p>This will cause TypeError complaining that tuples don't "support
item assignment." In other words, you can't change the elements in a
tuple because tuples are immutable. Later in the course, we'll see the
opposite -- <i>mutable</i> data structures.</p>

<h3 class="section_title">Recursive Lists</h3>
<p>Recall that the constructor and selectors for
<span class='code'>rlist</span>s are as follows:

<pre class='codemargin'>
empty_rlist = None

def rlist(first, rest=empty_rlist):
    return (first, rest)

def first(rlist):
    return rlist[0]

def rest(rlist):
    return rlist[1]
</pre>

<p>As you do the questions below, keep in mind that an rlist is an
abstract data type! In other words, your code should not assume that
rlists are implemented as tuples.</p>

<p>5.) It would be convenient if we had a way to convert from tuples to
rlists. Write a function <span class='code'>tup_to_rlist</span> that
does exactly that.</p>

<p><b>Hint</b>: if you are writing the function iteratively, it might
be helpful to reverse the tuple first.</p>

<pre class='codemargin'>
def tup_to_rlist(tup):
    """Converts a tuple to an rlist.

    &gt;&gt;&gt; tup = (1, 2, 3, 4)
    &gt;&gt;&gt; r = tup_to_rlist(tup)
    &gt;&gt;&gt; first(r)
    1
    &gt;&gt;&gt; first(rest(rest(r)))
    3
    &gt;&gt;&gt; r = tup_to_rlist(())
    &gt;&gt;&gt; r is empty_rlist
    True
    """
    "***YOUR CODE HERE ***"
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <pre class='codemargin'>
def tup_to_rlist(tup):
    if not tup:
        return empty_rlist
    return rlist(tup[0], tup_to_rlist(tup[1:]))
	  </pre>
    </div>
    <?php } ?>

<p>6.) Recall the sequence abstraction: a sequence has a finite
<b>length</b> and supports <b>element selection</b>. Implement the
<span class='code'>len_rlist(lst)</span> function, which calculates
the length of an rlist, and the <span class='code'>getitem_rlist(i,
lst)</span> function, which gets the <i>i</i>th item in the rlist.</p>

<pre class='codemargin'>
def len_rlist(lst):
    """Returns the length of the rlist.

    &gt;&gt;&gt; lst = tup_to_rlist((1, 2, 3, 4))
    &gt;&gt;&gt; len_rlist(lst)
    4
    &gt;&gt;&gt; lst = tup_to_rlist(())
    &gt;&gt;&gt; len_rlist(lst)
    0
    """
    "*** YOUR CODE HERE ***"

def getitem_rlist(i, lst):
    """Returns the ith item in the rlist. If the index exceeds the
    length of the rlist, return 'Error'.

    &gt;&gt;&gt; lst = tup_to_rlist((1, 2, 3, 4))
    &gt;&gt;&gt; getitem_rlist(0, lst)
    1
    &gt;&gt;&gt; getitem_rlist(3, lst)
    4
    &gt;&gt;&gt; getitem_rlist(4, lst)
    'Error'
    """
    "*** YOUR CODE HERE ***"
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <pre class='codemargin'>
def len_rlist(lst):
    if lst == empty_rlist:
        return 0
    return 1 + len_rlist(rest(lst))

def getitem_rlist(i, lst):
    "*** YOUR CODE HERE ***"
    if lst == empty_rlist:
        return 'Error'
    elif i == 0:
        return first(lst)
    else:
        return getitem_rlist(i - 1, rest(lst))
	  </pre>
    </div>
    <?php } ?>


<p>7.) At this point, we want to test your code for any data
abstraction violations. Change the constructors and selectors for
rlists to the following:</p>

<pre class='codemargin'>
empty_rlist = lambda x: x

def rlist(first, rest=empty_rlist):
    return lambda x: first if x == 'hi' else rest

def first(lst):
    return lst('hi')

def rest(lst):
    return lst('lol')
</pre>

<p>After the changes, try running the doctests again. Do your solutions
for Q5 and Q6 still work? If so, you have preserved abstraction!</p>

<h3 class="section_title">Strings</h3>

<p>8.) What would Python print?</p>

<pre class='codemargin'>
&gt;&gt;&gt; s = 'hello world!'
&gt;&gt;&gt; s[0]     # Q1
______
&gt;&gt;&gt; s[-1]    # Q2
______
&gt;&gt;&gt; s[100]   # Q3
______

&gt;&gt;&gt; s[1:-1]  # Q4
______
&gt;&gt;&gt; s[::-1]  # Q5
______
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <pre class='codemargin'>
1) 'h'
2) '!'
3) IndexError
4) 'ello world'
5) '!dlrow olleh'
	  </pre>
    </div>
    <?php } ?>

<pre class='codemargin'>
&gt;&gt;&gt; 'hello ' + 'world!'  # Q6
______
&gt;&gt;&gt; 'hello ' * 3         # Q7
______

&gt;&gt;&gt; 'ello' in s    # Q8
______
&gt;&gt;&gt; len(s)         # Q9
______
&gt;&gt;&gt; tuple(s)       # Q10
______
&gt;&gt;&gt; s.upper()      # Q11
______
&gt;&gt;&gt; s.split()      # Q12
______
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <pre class='codemargin'>
6) 'hello world!'
7) 'hello hello hello '
8) True
9) 12
10) ('h', 'e', 'l', 'l', o', ' ', 'w', 'o', 'r', 'l', 'd', '!')
11) 'HELLO WORLD!'
12) ['hello', 'world!']
	  </pre>
    </div>
    <?php } ?>

<p>9.) Write expressions so that the following results occur:</p>

<pre class='codemargin'>
&gt;&gt;&gt; s = 'hello world!'
&gt;&gt;&gt; s[:5]    # example
'hello'
&gt;&gt;&gt; EXPRESSION INVOLVING s HERE
'world'
&gt;&gt;&gt; EXPRESSION INVOLVING s HERE
'olleh'
&gt;&gt;&gt; EXPRESSION INVOLVING s HERE
'hlowrd'
&gt;&gt;&gt; EXPRESSION INVOLVING s HERE
'world hello!'
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <pre class='codemargin'>
1) s[6:-1]
2) s[4::-1]
3) s[::2]
4) s[6:-1] + s[5] + s[:5] + s[-1]
	  </pre>
    </div>
    <?php } ?>

<p>A couple of cool things about strings:</p>
<ol>
  <li>You can iterate over the <i>characters</i> of strings with
    <span class='code'>for</span> loops. This does not iterate over the
    words!</li>
  <li>You can cast things into strings by using the
    <span class='code'>str</span> constructor, like so:
    <span class='code'>str(123)</span> returns '123'.</li>
  <li>Strings have lots of handy <i>methods</i> (methods are sort of
    like functions -- we'll talk more about them later). You can find
    out what methods strings have by typing
    <span class='code'>dir(str)</span>. You can then use the methods
    using dot notation. For example,
    <span class='code'>'Hi There'.swapcase()</span> calls the
    <span class='code'>swapcase</span> method.</li>
</ol>

<h3 class="section_title">Sequences</h3>

<p>Python has some functions that are useful for working with sequences
like tuples and strings:</p>

<ul>
  <li><b>map</b>: applies a function to each element in a sequence</li>
  <li><b>filter</b>: keeps elements in a sequence only if they satisfy 
    a given predicate function</li>
  <li><b>reduce</b>: combines elements in a function using a given
     combiner</li>
</ul>

<p><span class='code'>map</span> and <span class='code'>filter</span>
don't actually return tuples -- instead, <span class='code'>map</span>
returns a <span class='code'>map</span> object, and
<span class='code'>filter</span> returns a
<span class='code'>filter</span> object, neither of which you can
index. You can however convert each one into a tuple by using the
<span class='code'>tuple</span> constructor, as demonstrated below.</p>

<p>Try these out!</p>
<pre class='codemargin'>
&gt;&gt;&gt; map(lambda x: x**2, (1, 2, 3, 4))
______
&gt;&gt;&gt; tuple(map(lambda x: x**2, (1, 2, 3, 4)))
______

&gt;&gt;&gt; filter(lambda x: x % 2 == 0, (1, 2, 3, 4))
______
&gt;&gt;&gt; tuple(filter(lambda x: x % 2 == 0, (1, 2, 3, 4)))
______

&gt;&gt;&gt; from functools import reduce
&gt;&gt;&gt; reduce(lambda a, b: a + b, (1, 2, 3, 4, 5))
______
&gt;&gt;&gt; reduce(lambda a, b: b + a, 'hello world!')
______
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <pre class='codemargin'>
1) &lt;map object at ...&gt;
2) (1, 4, 9, 16)
3) &lt;filter object at ...&gt;
4) (2, 4)
5) 15
6) '!dlrow olleh'
	  </pre>
    </div>
    <?php } ?>

<p>
  <?php if ($CUR_DATE > $RELEASE_DATE) { ?>
  <script src="http://code.jquery.com/jquery-latest.js"></script>
  <?php } ?>
</p>
  <script>
      <?php for ($i = 0; $i < $q_num; $i++) { ?>
    $("#toggleButton<?php echo $i; ?>").click(function () {
      $("#toggleText<?php echo $i; ?>").toggle();
    });
      <?php } ?>
    </script>
</body>
</html>
