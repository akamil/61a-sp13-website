<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
          "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"> 
  <head>
    <meta name="description" content ="CS61A: Structure and Interpretation of Computer Programs" /> 
    <meta name="keywords" content ="CS61A, Computer Science, CS, 61A, Programming, Berkeley, EECS" /> 
    <meta name="author" content ="Amir Kamil, Hamilton Nguyen, Joy Jeng, Keegan Mann, Stephen Martinis, Albert Wu, Julia Oh, Robert Huang, Mark Miyashita, Sharad Vikram, Soumya Basu, Richard Hwang" /> 
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/> 
    <style type="text/css">@import url("https://inst.eecs.berkeley.edu/~cs61a/su12/lab/lab_style.css");</style>
    
    <title>CS 61A Spring 2013: Lab 4</title> 

    <?php
    /* So all of the PHP in this file is to allow for this nice little trick to 
     * help us avoid having two versions of the questions lying around in the 
     * repository, which often leads to the two versions going out of sync which 
     * leads to annoyance for students.
     *
     * The idea's pretty simple for the PHP part, just simply have two dates: 
     *
     *    1. The current date
     *    2. The date the solutions should be released
     *
     * Using these, we now wrap our solutions in a simple PHP if statement that 
     * checks if the date is past the release date and only includes the code on 
     * the page displayed (what the server gives back to the browser) if the 
     * solutions are supposed to be released.
     *
     * We also use some PHP to create unique IDs for each of the show/hide 
     * buttons and solution divs, which are then used in the PHP generated 
     * jQuery code that we use to create the nice toggling effect.
     *
     * I apologize if the PHP/jQuery is really offensively bad, this is 
     * literally the most I've written of either for a single project so far.
     * Comments/suggestions are most welcome!
     *
     * - Tom Magrino (tmagrino@berkeley.edu)
     */
    $BERKELEY_TZ = new DateTimeZone("America/Los_Angeles");
    $RELEASE_DATE = new DateTime("02/21/2013", $BERKELEY_TZ);
    $CUR_DATE = new DateTime("now", $BERKELEY_TZ);
    $q_num = 0; // Used to make unique ids for all solutions and buttons
    ?>
  </head> 
  
  <body style="font-family: Georgia,serif;"> 

<h1>CS61A Lab 4: Recursion, and General Debugging</h1>
<h3>Week 4, 2013</h3>
<h3 class="section_title">Iteration and Recursion</h3>

<p>For the following 2 questions, write both an iterative solution (uses a <span
class="code">while</span> or <span class="code">for</span> loop) and a recursive
solution (no <span class="code">while</span> or <span class="code">for</span>
loops).</p>

<p>1.) Write a function <span
class="code">add_ten</span> which takes 2 integers <span class="code">n</span> and <span class="code">x</span>, and returns <span class="code">x + n*10</span>. DO NOT simply put <span
class="code">return x + 10*n</span> as the answer. Instead, try to write 2 versions of the function that implement it iteratively and recursively. We aren't testing you on your arithmetic skills.</p>

<p>2.) The greatest common divisor of two positive integers <span
class="code">a</span> and <span class="code">b</span> is the largest integer
which evenly divides both numbers (with no remainder). Euclid, a Greek
mathematician in 300 BC, realized that the greatest common divisor of <span
class="code">a</span> and <span class="code">b</span> is the smaller value if it
evenly divides the larger value or the same as the greatest common divisor of
the smaller value and the remainder of the larger value divided by the smaller
value. So if <span class="code">a</span> is greater than <span
class="code">b</span> and <span class="code">a</span> is not divisible by <span
class="code">b</span> then:</p>

<pre class="codemargin">
gcd(a, b) == gcd(b, a % b)
</pre>

<p>Write the <span class="code">gcd</span> function using Euclid's
algorithm.</p>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p>
	  <pre class="codemargin">
1.) 
def add_ten_iter(x, n):
    i, sum = 1, x
    while i <= n:
        sum += 10
        i += 1
    return 1
   
def add_ten_rec(x, n):
    if n == 0:
        return x
    return 10 + add_ten_rec(x, n-1)


def gcd_iter(a, b):
    if a < b:
        return gcd_iter(b, a)
    while a > b and not a % b == 0:
        a, b = b, a % b
    return b

def gcd_rec(a, b):
    if a < b:
        return gcd_rec(b, a)
    if a > b and not a % b == 0:
        return gcd_rec(b, a % b)
    return b
	</pre>
	  </p>
    </div>
    <?php } ?>

<h3 class="section_title">Recursion</h3>
<ol style = "padding-left:20px">
<li>
<p>Rewrite the following function recursively.</p>
<pre>
def func(a, b):
    i = 1
    while i &lt;= a:
        print(i*b)
        i += 1
</pre>
</li>
<li>
Now we're going to approximate the sine trigonometric function using 2 useful facts. One is that sin x is approximately equal to x as x gets small (for this problem, below 0.0001), and the other is the trigonometric identity
<p><img src="sin.gif" /></p>
<p>Using those two facts, write a function <span class="code">sin</span> that returns the sine of a value in radians.</p>
</li>
<li>
<p>  Consider an insect in a MxN grid. The insect starts at the top left corner, (0,0), and wants to end up at the bottom right corner, (M-1,N-1). The insect is only capable of moving right or down. Write a function count_paths that takes a grid length and width and returns the number of different paths the insect can take from the start to the goal. [There is an analytic solution to this problem, but try to answer it procedurally using recursion]. </p>
<img src="grid.jpg"/>
<p>
For example, the 2x2 grid has a total of two ways for the insect to move from the start to the goal. For the 3x3 grid, the insect has 6 different paths (only 3 are shown above).
</p>
</p>
</li>
</ol>
  <?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p>
    <pre class="codemargin">
1.)
def func_rec(a, b):
    if a == 0:
        return
    func_rec(a-1, b)
    print(a*b)
    
2.)
def sine(x):
    if abs(x) < 0.0001:
        return x
    return (3*sine(x/3) - 4*pow(sine(x/3), 3))
    
3.)
def paths(x, y):
    if x == 1 or y == 1:
        return 1
    return paths(x - 1, y) + paths(x, y - 1)
  </pre>
    </p>
    </div>
    <?php } ?>

<h3 class="section_title">Debugging</h3>
<ol style = "padding-left:20px">
<li>
<p>Whenever Python encouters a bug in your code, it will print a list of error messages. This
printed statement is known as a <i>traceback</i>. A traceback might look scary to the 
untrained eye, but it is actually a very useful debugging tool!</p>

<p>A sample traceback looks like this:</p>

<pre>
Traceback (most recent call last):
  File "bug1.py", line 22, in func
      g = bug()
  File "bug2.py", line 3, in bug
      buggy = buggy + 1
NameError: name 'buggy' is not defined
</pre>

<p>A traceback displays all the <i>function calls</i> that led up to the error. You can think of this in terms of an environment diagram where the traceback displays all of the frames that we have created up to this point that have not returned a value. Notice that the message tells you that the "most recent call" is displayed "last", at the bottom of the message. We can think of this "last" call as being the current frame that we are in.</p>

<p>You may have noticed that the traceback is organized in "couplets." Each couplet is composed of two
    lines: the location; and the actual code</p>

<p>The location is the first line in the couplet:</p>
<pre>
  File "<b>file name</b>", line <b>number</b>, in <b>function name</b>
</pre>
<p>This line provides you with three bits of information:</p>
<ul>
    <li><b>file name:</b> the name of the file where this function call is located</li>
    <li><b>line number:</b> the line number in the file where the error is located</li>
    <li><b>function name:</b> the name of the function that houses the error</li>
</ul>

<p>From this line alone, you should be able to locate the line where the error occurs, in case you need 
    reference it in more detail.</p>

<p>The second line in the couplet is the code line:</p>
<pre>
    <b>line of code here</b>
</pre>
<p>This line allows you to see the piece of code that triggered the error just by looking at the traceback.
    That way, you can quickly reference the code and guess why the error occured.</p>

<p>The very last line in a traceback is the error type:</p>
<pre>
    Error type: Error message
</pre>
<p>The error type is usually named to be descriptive (such as SyntaxError or IndexError). The error message is a
more detailed (but still brief) description of the error.</p>

<p>Now that you know how to read a traceback, you are well-equipped to handle any bugs that come your way.
    The following are some common bugs that occur frequently (many of you might have seen them before). Try
    to debug the code!</p>
    
<p>You can copy the starter code (for the debugging and list portions of the lab) 
into your current directory to produce the traceback. </p>

<pre>
cp ~cs61a/lib/lab/lab04/lab4.py .
</pre>

<p>Feel free to do so, but copying and pasting from a webpage doesn't always yield the results
you would expect!</p>

<pre>
def one(mississippi):
    """
    >>> one(1)
    1
    >>> one(2)
    4
    >>> one(3)
    6
    """
    if mississippi == 1:
        return misssissippi
    elif mississippi == 2:
        return 2 + mississippi
    elif mississippi = 3:
        return 3 + mississippi

def two(two):
    """
    >>> two(5)
    15
    >>> two(6)
    18
    """
    one = two
    two = three
    three = one
    return three + two + one

def three(num):
    """
    >>> three(5)
    15
    >>> three(6)
    21
    """
    i, sum = 0, 0
    while num > i:
        sum += num
    return sum

def four(num):
    """
    >>> four(5)
    16
    >>> four(6)
    32
    """
    if num == 1:
        return num
    return four(num - 1) + four(num)
</pre>

<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
    <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
    <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
      <p><pre>
def one(mississippi):
    if mississippi == 1:
        return misssissippi                 # extra 's' in mississippi
    elif mississippi == 2:
        return 2 + mississippi
    elif mississippi = 3:                   # should be ==, not =
        return 3 + mississippi

def two(two):
    one = two
    two = three                             # the variable three is undefined
    three = one
    return three + two + one

def three(num):
    i, sum = 0, 0
    while num > i:
        sum += num                          # our loop never terminates because we don't increment i/decrement num!
    return sum

def four(num):
    if num == 1:
        return num
    return four(num - 1) + four(num - 1)    # second recursive call doesn't decrement num, this yields infinite recursion!
      </pre></p>
    </div>
    <?php } ?>

<p>Finally, you should almost always run the doctests we provide you <i>from the terminal</i>,
instead of copying and pasting the tests into an interpreter. This avoids typing mistakes, is a lot
faster, and achieves the same result.</p>
</li>
<li>
<p>
  For this example, we are going to be debugging some code that has been given to us, as well as writing some test cases to make sure that the function is doing what we want it to do. Copy the template for this file by running the following from your terminal:
</p>
<pre>
  cp ~cs61a/lib/lab/lab04/anagrams.py .
</pre>
<p>
  Let's open the file in your favorite text editor (probably Emacs if you're on the lab machines) and try to understand the code. You do not have to understand how the function <span class="code">remove(letter, word)</span> works; we will learn that later, but you should try to understand what the rest of the functions are doing based off of the docstrings. We're given several functions that will help us find out whether two words are anagrams of one another, that is, we want to know if we can use all the original letters of <span class="code">w1</span> exactly once to form <span class="code">w2</span> and vice versa. We can assume that all of the functions written below <span class="code">contains</span> work as described and that they work correctly. Our job is to figure out if the <span class="code">contains</span> function works correctly, and if it does not, fix it so that it does. Let's open our file in an interactive Python session by running:
</p>
<pre>
  python3 -i anagrams.py
</pre>
<p>
  Play around with the functions that we gave you and verify that they do indeed work. It also might be a good idea to run our doctests using the command: <span class="code">python3 -m doctest anagrams.py</span>.
</p>
<pre>
**********************************************************************
File "./anagrams_bad.py", line 60, in anagrams_bad.anagrams
Failed example:
    anagrams("domo", "doom") # but for the record, Domo is awesome.
Expected:
    True
Got:
    False
**********************************************************************
File "./anagrams_bad.py", line 4, in anagrams_bad.contains
Failed example:
    contains("cute", "computer")
Expected:
    True
Got:
    False
**********************************************************************
File "./anagrams_bad.py", line 8, in anagrams_bad.contains
Failed example:
    contains("car", "race")
Expected:
    True
Got:
    False
**********************************************************************
File "./anagrams_bad.py", line 10, in anagrams_bad.contains
Failed example:
    contains("hi", "high")
Expected:
    True
Got:
    False
**********************************************************************
</pre>
<p>
  Whoa, we are failing 4 doctests, not good, not good at all. Let's take a closer look at why. In the three doctests for <span class="code">contains</span>, we see that the <span class="code">contains</span> is returning <span class="code">False</span> when it is expecting <span class="code">True</span>. Take a couple of minutes to think of additional test cases that would return <span class="code">False</span> when we expect <span class="code">True</span> and vice versa. Compare test cases with your neighbors to see if there are any that either of you missed.
</p>
<p>
  Now that we have found a set of test cases that are yielding incorrect results, we can start to figure out if we need to change existing base cases, add more base cases, or to alter our recursive case(s). Let's take a closer look at our base cases. Our function's base cases only seem to return <span class="code">False</span>! That means that no matter what we input, this function will always return <span class="code">False</span>. That's not what we want. Add a base case that might help alleviate this problem.
</p>
<p>
  Hopefully, we found an edge case in the previous exercise and now our function correctly returns the value that we want. However, all 4 of our doctests are still failing! We should take a look at our recursive cases. Remember in recursion, the idea is that we should always be working towards our base case. Otherwise, our function will continuously call itself with the same input causing our function to fall into what we call "infinite recursion". To avoid infinite recursion, always make sure that the input is working towards satisfying the base case. Right now, our function is continuously checking to see if the first letter of <span class="code">w1</span> is in <span class="code">w2</span>. If it is, we are recursively calling <span class="code">contains</span> while removing the first letter of <span class="code">w1</span> from <span class="code">w2</span>. Walk through this process using the last doctest, <span class="code">contains("hi", "high")</span>, as an example to see what exactly the code does. Writing out each function call on a piece of paper has been said to help. Are we recursively calling <span class="code">contains</span> with the desired inputs? See if you can fix the recursive case. If you are having trouble, talk with your neighbors or lab assistants to see if they can point you in the right direction.
</p>
<p>
  Great, you fixed the recursive case! Let's run the doctests again to make sure everything is passing.
</p>
<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
  <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
  <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
    <p>
      <pre class="codemargin">
def contains(w1, w2):
    if len(w1) == 0:
        return True
    if len(w2) == 0:
        return False
    if has_letter(first(w1), w2):
        return contains(rest(w1), remove(first(w1), w2))
    return False
      </pre>
    </p>
  </div>
<?php } ?>
</li>
<li>
  Thanks to your awesome TA, Albert, <a href="http://www-inst.eecs.berkeley.edu/~cs61a/fa12/pdfs/debugging.pdf">here</a> is a pdf of general debugging tips. This guide will help you read the traceback message, decrypt error messages, and identify common errors that Python programmers run into often. Make sure you read through this guide; it will make writing and debugging programs much easier!
</li>
</ol>

<h3 class="section_title">Extra Recursion Examples</h3>
<ol style = "padding-left:20px">
<li>
  <p>
    Given the following functions <span class="code">first(word)</span>, <span class="code">second(word)</span>, and <span class="code">rest(word)</span> see if you can define a recursive function <span class="code">count_hi(phrase)</span> that takes in a string of any length and returns the number of times the phrase "hi" appears within.
  </p>
  <pre>
def first(word):
    return word[0]
        
def second(word):
    return word[1]
            
def rest(word):
    return word[1:]
  </pre>
  <p>
    Here are some doctests to help you out:
  </p>
  <pre>
>>> count_hi("hihihihi")
4
>>> count_hi("hhjfdkfj")
0
>>> count_hi("")
0
>>> count_hi("hih")
1
  </pre>
</li>
<li>
  <p>
    Given a function <span class="code">is_vowel(char)</span> that takes in a single character and returns a boolean stating whether or not it is a vowel or not, define a recursive function <span class="code">remove_vowels(word)</span> that removes all vowels from the word. Feel free to use the functions <span class="code">first(word)</span> or <span class="code">rest(word)</span> from the previous problem.
  </p>
  <pre>
def is_vowel(char):
    return char == 'a' or char == 'e' or char == 'i' or char =='o' or char == 'u'
  </pre>
  <p>
    Here are a few doctests to help you out, although, the functionality of this code should be somewhat obvious by the name...
  </p>
  <pre>
>>> remove_vowels("hi my name is mark")
'h my nm s mrk'
>>> remove_vowels("aeiou")
''
>>> remove_vowels("is y a vowel?")
's y  vwl?'
  </pre>
</li>
<li>
  <p>
    By now you're an expert on recursion right? Let's write a recursive function <span class="code">pair_star(phrase)</span> that takes in a phrase and returns the same phrase where identical characters that are adjacent in the original phrase are separated with the character "*". Phrases without identical characters adjacent to one another should just return the original string. You can concatenate two strings together using the "+" operator. For example, "hello" + " " + "world" will yield the result 'hello world'
  </p>
  <pre>
>>> pair_star("hiihhi")
'hi*ih*hi'
>>> pair_star("woodlands have squirrels")
'wo*odlands have squir*rels'
>>> pair_star("h")
'h'
  </pre>
</li>
</ol>
<?php if ($CUR_DATE > $RELEASE_DATE) { ?>
  <button id="toggleButton<?php echo $q_num; ?>">Toggle Solution</button>
  <div id="toggleText<?php echo $q_num++; ?>" style="display: none">
    <p>
      <pre class="codemargin">
1.)
def count_hi(phrase):
    if len(phrase) <= 1:
        return 0
    elif first(phrase) == 'h' and second(phrase) == 'i':
        return 1 + count_hi(rest(phrase))
    else:
        return count_hi(rest(phrase))
        
2.)
def remove_vowels(word):
    if word == '':
        return ''
    elif is_vowel(first(word)):
        return remove_vowels(rest(word))
    else:
        return first(word) + remove_vowels(rest(word))
        
3.)
def pair_star(phrase):
    if len(phrase) <= 1:
        return phrase
    elif first(phrase) == second(phrase):
        return first(phrase) + "*" + pair_star(rest(phrase))
    else:
        return first(phrase) + pair_star(rest(phrase))
      </pre>
    </p>
  </div>
<?php } ?>

<p>
  <?php if ($CUR_DATE > $RELEASE_DATE) { ?>
  <script src="http://code.jquery.com/jquery-latest.js"></script>
  <?php } ?>
</p>
  <script>
      <?php for ($i = 0; $i < $q_num; $i++) { ?>
    $("#toggleButton<?php echo $i; ?>").click(function () {
      $("#toggleText<?php echo $i; ?>").toggle();
    });
      <?php } ?>
    </script>
</body>
</html>
