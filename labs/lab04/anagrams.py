def contains(w1, w2):
    """ Checks whether all the letters in w1 are also in w2; in other words,
    whether you can use the letters in w2 to spell w1.
    >>> contains("cute", "computer")
    True
    >>> contains("food", "foo")
    False
    >>> contains("car", "race")
    True
    >>> contains("hi", "high")
    True
    """
    if len(w2) == 0:
        return False
    if has_letter(first(w1), w2):
        return contains(w1, remove(first(w1), w2))
    return False
    
# Everything below this comment is considered correct.
# The bugs (if there are any) lie within the 'contains' function described above.
        
def first(word):
    """ Returns the first letter of the given string. """
    return word[0]
        
def second(word):
    """ Returns the second letter of the given string. """
    return word[1]
            
def rest(word):
    """ Returns the given string, except the first character. """
    return word[1:]
    
def has_letter(letter, word):
    """ Checks whether the given letter is within the word.
    >>> has_letter('b', 'blah')
    True
    >>> has_letter('t', 'blah')
    False
    """
    if len(word) == 0:
        return False
    if first(word) == letter:
        return True
    return has_letter(letter, rest(word))
    
def remove(letter, word):
    """ Removes the first occurrance of the given letter from the word.
    >>> remove('p', 'apples')
    'aples'
    """
    assert has_letter(letter, word), "Cannot remove {0} from {1}".format(letter, word)
    return word.replace(letter, "", 1)
    
def anagrams(w1, w2):
    """ Returns whether or not w1 and w2 are anagrams of one another.
    >>> anagrams("domo", "doom") # but for the record, Domo is awesome.
    True
    >>> anagrams("car", "race")
    False
    """
    return contains(w1, w2) and contains(w2, w1)