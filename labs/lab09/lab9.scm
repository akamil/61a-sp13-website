; Your name
; TA name
; CS61A Lab 9

; Warmup #1
(define (factorial_recursive x)
    0)

; Warmup #2
(define (factorial_iterative x)
    0)


; HOF #1
(define (cube x)
  (* x x x))

; Returns the sum of terms from a to b.
(define (sum term a next b)
    (if (> a b)
        0
        (+ (term a) (sum term (next a) next b))))

(define (simpson_approx f a b n)
  0)


; HOF #2
(define (product_recursive term a next b)
  0)

(define (product_iterative term a next b)
  0)


; HOF #3
(define (accumulate combiner null-value term a next b)
  0)

(define (sum_by_accum term a next b) 0)
(define (product_by_accum term a next b) 0)


; Lists #1
(define structure 0)


; Lists #2
(define (interleave lst1 lst2)
  (cons 0 0))

; Lists #3
(define (remove item lst)
  (cons 0 0))

; Lists #4
(define (all_satisfies lst pred)
  #t)

; Lists #5
(define (repeat_els lst pred)
  (cons 0 0))
