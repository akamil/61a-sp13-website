;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: And Rain Will Make The Flowers Grow
;;;
;;; Description:
;;;   <Bursting into bloom
;;;    A bouquet of gracideas
;;;    Gift of recursion.>

(define (repeat k fn)
	(if (> k 0)
		(begin (fn) (repeat (- k 1) fn))
			'done))

(define (leaves size col outline)
	(color outline) (begin_fill)
	(repeat 2 (lambda () 
		(repeat 3 (lambda () (circle size 60) (lt 120) (circle size 60))) (rt 60)))
			(color col) (end_fill))

(define inside-pink "#ff9cb5")
(define outside-pink "#ff6b8c")
(define outline-green "#316339")
(define dark-green "#49c452")
(define light-green "#adff63")

(define (gracidea size depth)
	(cond ((= depth 0) (lt 120))
		  ((= depth 1)  (leaves size light-green dark-green)  (lt 30) 
						(leaves size inside-pink outside-pink) (rt 30))
		  (else (leaves size dark-green outline-green) (repeat 2 (lambda () 
			(repeat 3 (lambda () 
			(penup) (circle size 60) (pendown)
			(lt 20) (gracidea (/ size 2) (- depth 1)) (rt 20)
			(penup) (circle size 60))) (lt 60) (pendown)))
			(gracidea (/ size 2.8) 1) (rt 20) (gracidea (/ size 5.6) 1) (rt 100))))

(define (draw)
  (speed 0) (gracidea 168 4))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
