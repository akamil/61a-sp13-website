;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: recursion springs eternal
;;;
;;; Description:
;;;   Recursive flowers
;;;   drawn in an infinite loop--
;;;   Eternal spring.

(define (draw)
  (hideturtle)
  (speed 0)
  (seth 0)
  (flower 10 5 1))
  
(define (flower total max-value counter)
  (cond ((or (< total 0) (= max-value 0)) 
                     (begin (color "red") (circle total 270) 
                            (penup) (fd 10) (pendown) 
                            0))
		((= total 0) (begin (color "black" )
                            (if (= 0 (modulo counter 2)) 
                                (begin (penup) (setpos 0 (* counter 5)) (pendown)))
                            (if (< (readh) 90)
                                (begin (seth 0) (fd (+ 40 (* counter 2))) (lt 20))
                                (begin (seth 45) (fd 40) (rt 20)))
                            1))
		(else (+ (flower (- total max-value) max-value counter)
				 (flower total (- max-value 1) (+ 1 counter))))))
                 
; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
