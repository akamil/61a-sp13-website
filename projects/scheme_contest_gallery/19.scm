;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: Eleisa's Miracle
;;;
;;; Description:
;;;    A useless item,
;;;    Don't buy it while playing in
;;;    The League of Legends

(define (draw)
(fd 100)
(rt 90)
(fd 100)
(rt 270)
(rt 270)
(speed 1337)
(color 'white)
(rt 90)
(fd 1000)
(rt 270)
(begin_fill)
(color 'black)
(circle 1500)
(end_fill)
(rt 270)
(fd 1000)
(rt 90)


(define (heart-curve1 size level)
(cond ((= level 0) (fd size))
(else (heart-curve1 size (- level 1))
(rt 90)
(color 'cyan)
(heart-curve2 size (- level 1))
(lt 90))))

(define (heart-curve2 size level)
(cond ((= level 0) (fd size))
(else (heart-curve2 size (- level 1))
(rt 90)
(color 'red)
(heart-curve3 size (- level 1))
(lt 90))))

(define (heart-curve3 size level)
(cond ((= level 0) (fd size))
(else (heart-curve3 size (- level 1))
(rt 90)
(color 'purple)
(heart-curve4 size (- level 1))
(lt 90))))

(define (heart-curve4 size level)
(cond ((= level 0) (fd size))
(else (heart-curve4 size (- level 1))
(rt 90)
(color 'red)
(heart-curve5 size (- level 1))
(lt 90))))

(define (heart-curve5 size level)
(cond ((= level 0) (fd size))
(else (heart-curve5 size (- level 1))
(rt 90)
(color 'blue)
(heart-curve6 size (- level 1))
(lt 90))))

(define (heart-curve6 size level)
(cond ((= level 0) (fd size))
(else (heart-curve6 size (- level 1))
(rt 90)
(color 'blue)
(heart-curve7 size (- level 1))
(lt 90))))

(define (heart-curve7 size level)
(cond ((= level 0) (fd size))
(else (heart-curve7 size (- level 1))
(rt 90)
(color 'pink)
(heart-curve8 size (- level 1))
(lt 90))))

(define (heart-curve8 size level)
(cond ((= level 0) (fd size))
(else (heart-curve8 size (- level 1))
(rt 90)
(color 'red)
(heart-curve1 size (- level 1))
(lt 90))))

(heart-curve1 10 1)
(heart-curve2 10 2)
(heart-curve3 10 3)
(heart-curve4 10 4)
(heart-curve5 10 5)
(heart-curve6 10 6)
(heart-curve7 10 7)
(heart-curve8 10 9)
(hideturtle))
; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
