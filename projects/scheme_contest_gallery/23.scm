;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: <Majoras Mask>
;;;
;;; Description:
;;;   <Little Goblin Here
;;;    Now Wearing Majoras Mask
;;;    Creating Chaos>

(define (draw)
(speed 10)
(pu)
(background)
(rt 90)
(fd 200)
(rt 90)
(pd)
(gspiral oddogon 25 30 12)
(rt 97)
(gspiral shuriken 40 30 12)
(lt 97)
(gspiral oddogon 20 30 12)
(rt 97)
(gspiral shurriken 25 30 12)
(spiral octogon 20 30 12)
(sspiral octogon 20 30 20)
(spiral octogon 10 30 12)
(rt 113)
(pu)
(fd 400)
(pd)
(lt 90)
(gspiral2 oddogon2 25 30 12)
(lt 97)
(gspiral2 shuriken2 40 30 12)
(rt 97)
(gspiral2 oddogon2 20 30 12)
(lt 97)
(gspiral2 shurriken2 25 30 12)
(spiral2 octogon2 20 30 12)
(sspiral2 octogon2 20 30 20)
(spiral2 octogon2 10 30 12)
(lt 113)
(pu)
(fd 200)
(rt 180)
(s1 30)
(fd 30)
(s2 30)
(fd 30)
(s1 30)
(fd 30)
(s2 30)
(fd 30)
(s1 30)
(fd 30)
(s2 30)
(fd 30)
(s1 30)
(fd 30)
(s2 30)
(fd 30)
(s1 30)
(fd 30)
(s2 30)
(fd 30))

(define (octogon side)
 (fd side)(rt 45)(color "green")(end_fill)(color "darkgreen")(fd side)(rt 45)(begin_fill)(fd side)(rt 45)(fd side)(rt 45)(fd side)(rt 45)(color "red")(end_fill)
 (color "green")(begin_fill)(fd side)(rt 45)(fd side)(rt 45)(color "yellow")(end_fill)(begin_fill)(fd side)(rt 45))

(define (shuriken side)
 (color "darkblue")(begin_fill)(fd (* side 1.414213))(lt 45)(fd (* side 1.414213))(rt 135)(fd (* side 1.414213))(lt 45)(color "purple")(end_fill)(begin_fill)
 (fd (* side 1.414213))(rt 135)(fd (* side 1.414213))(lt 45)(color "darkblue")(end_fill)(color "gold")(fd (* side 1.414213))(rt 135)(fd (* side 1.414213))
 (lt 45)(fd (* side 1.414213))(rt 135))
(define (shurriken side)
 (color "darkred")(begin_fill)(fd (* side 1.414213))(lt 45)(fd (* side 1.414213))(rt 135)(fd (* side 1.414213))(lt 45)(color "purple")(end_fill)(begin_fill)
 (fd (* side 1.414213))(rt 135)(fd (* side 1.414213))(lt 45)(color "darkred")(end_fill)(color "green")(fd (* side 1.414213))(rt 135)(fd (* side 1.414213))
 (lt 45)(fd (* side 1.414213))(rt 135))

(define (oddogon side)
 (color "darkblue")(begin_fill)(fd (* side 1.414213))(lt 45)(fd (* side 1.414213))(rt 90)(fd (* side 1.414213))(lt 45)(end_fill)(color "gold")(begin_fill)
 (fd (* side 1.414213))(rt 90)(fd (* side 1.414213))(lt 45)(fd (* side 1.414213))(rt 90)(fd (* side 1.414213))(lt 45)(color "purple")(end_fill)(begin_fill)
 (fd (* side 1.414213))(rt 90)(fd (* side 1.414213))(lt 45)(fd (* side 1.414213))(rt 90)(fd (* side 1.414213))(lt 45)(fd (* side 1.414213))(rt 90)
 (color "darkblue")(end_fill)(color "gold")(fd (* side 1.414213))(lt 45)(fd (* side 1.414213))(rt 90)(fd (* side 1.414213))(lt 45)(fd (* side 1.414213))(rt 90))

(define (octogon2 side)
 (fd side)(lt 45)(color "green")(end_fill)(color "darkgreen")(fd side)(lt 45)(begin_fill)(fd side)(lt 45)(fd side)(lt 45)(fd side)(lt 45)(color "red")(end_fill)
 (color "green")(begin_fill)(fd side)(lt 45)(fd side)(lt 45)(color "yellow")(end_fill)(begin_fill)(fd side)(lt 45))

(define (shuriken2 side)
 (color "darkblue")(begin_fill)(fd (* side 1.414213))(rt 45)(fd (* side 1.414213))(lt 135)(fd (* side 1.414213))(rt 45)(color "purple")(end_fill)(begin_fill)
 (fd (* side 1.414213))(lt 135)(fd (* side 1.414213))(rt 45)(color "darkblue")(end_fill)(color "gold")(fd (* side 1.414213))(lt 135)(fd (* side 1.414213))
 (rt 45)(fd (* side 1.414213))(lt 135))
(define (shurriken2 side)
 (color "darkred")(begin_fill)(fd (* side 1.414213))(rt 45)(fd (* side 1.414213))(lt 135)(fd (* side 1.414213))(rt 45)(color "purple")(end_fill)(begin_fill)
 (fd (* side 1.414213))(lt 135)(fd (* side 1.414213))(rt 45)(color "darkred")(end_fill)(color "green")(fd (* side 1.414213))(lt 135)(fd (* side 1.414213))
 (rt 45)(fd (* side 1.414213))(lt 135))

(define (oddogon2 side)
 (color "darkblue")(begin_fill)(fd (* side 1.414213))(rt 45)(fd (* side 1.414213))(lt 90)(fd (* side 1.414213))(rt 45)(end_fill)(color "gold")(begin_fill)
 (fd (* side 1.414213))(lt 90)(fd (* side 1.414213))(rt 45)(fd (* side 1.414213))(lt 90)(fd (* side 1.414213))(rt 45)(color "purple")(end_fill)(begin_fill)
 (fd (* side 1.414213))(lt 90)(fd (* side 1.414213))(rt 45)(fd (* side 1.414213))(lt 90)(fd (* side 1.414213))(rt 45)(fd (* side 1.414213))(lt 90)
 (color "darkblue")(end_fill)(color "gold")(fd (* side 1.414213))(rt 45)(fd (* side 1.414213))(lt 90)(fd (* side 1.414213))(rt 45)(fd (* side 1.414213))(lt 90))

(define (s1 side)
 (lt 22)
 (color "darkblue")(begin_fill)
 (fd (* side 1.414213))(lt 45)(fd (* side 1.414213))(rt 135)
 (fd (* side 1.414213))(lt 45)(fd (* side 1.414213))(rt 135)
 (color "purple")(end_fill)(color "purple")(begin_fill)
 (fd (* side 1.414213))(lt 45)(fd (* side 1.414213))(rt 135)
 (fd (* side 1.414213))(lt 45)(fd (* side 1.414213))(rt 135)
 (color "darkblue")(end_fill)(color "purple")(begin_fill)
 (rt 22))
(define (s2 side)
 (lt 22)
 (color "purple")(begin_fill)
 (fd (* side 1.414213))(lt 45)(fd (* side 1.414213))(rt 135)
 (fd (* side 1.414213))(lt 45)(fd (* side 1.414213))(rt 135)
 (color "darkblue")(end_fill)(color "darkblue")(begin_fill)
 (fd (* side 1.414213))(lt 45)(fd (* side 1.414213))(rt 135)
 (fd (* side 1.414213))(lt 45)(fd (* side 1.414213))(rt 135)
 (color "purple")(end_fill)(color "darkblue")(begin_fill)
 (rt 22))

(define (spiral shape side angle n)
 (if (> n 0) (begin (shape side) (rt angle) (spiral shape side angle (- n 1))) nil))

(define (sspiral shape side angle n)
 (if (> n 0) (begin (shape side) (rt angle) (sspiral shape (- side (/ 1 2)) angle (- n 1))) nil))

(define (gspiral shape side angle n)
 (if (> n 0) (begin (shape side) (rt angle) (gspiral shape (+ side 2) angle (- n 1))) nil))

(define (spiral2 shape side angle n)
 (if (> n 0) (begin (shape side) (lt angle) (spiral2 shape side angle (- n 1))) nil))

(define (sspiral2 shape side angle n)
 (if (> n 0) (begin (shape side) (lt angle) (sspiral2 shape (- side (/ 1 2)) angle (- n 1))) nil))

(define (gspiral2 shape side angle n)
 (if (> n 0) (begin (shape side) (lt angle) (gspiral2 shape (+ side 2) angle (- n 1))) nil))

(define (background) (color "black")(begin_fill)(fd 450)(rt 90)(fd 550)(rt 90)(fd 900)(rt 90)(fd 1100)(rt 90)(fd 900)(rt 90)(fd 550)(end_fill)
 (rt 90)(fd 450)(rt 180))


; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
