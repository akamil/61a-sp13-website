;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: <Your title here: a masterpiece of modern art>
;;;
;;; Description:
;;;   <Zeno was a wise man 
;;;    Ramanajun was wiser
;;;    Learn to add, man >

(define (draw)
  ; *YOUR CODE HERE*
  (define (draw-helper length first)
    (cond ((= first #t) (pd)(forward length)(pu)(rt 90)(forward 5) (rt 90) (draw-helper (/ length 2) #f))
          ((= first #f) (pd)(forward length)(cond ((> length (/ 1 1024)) (draw-helper (/ length 2) #f))
                                                  ((< length (/ 1 1024)) nil)))))
  (draw-helper 100 #t))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)


