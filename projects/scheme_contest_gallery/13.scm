
;;;
;;; Title: 
;;;   Abstract Tower
;;;
;;; Description:
;;;   Recursive iteration, loop formation. Identical symmetry, mirrors perfectly.







(speed 0)
(setposition 0 30)
(clear)
(define (repeat k fn)
     (if (> k 0)
     (begin (fn) (repeat (- k 1) fn))
     'done))
(define (square fn)
  (repeat 4 (lambda () (fn) (lt 90))))
(define (jumb d k)
  (square (lambda () 
    (if (= k 1)
      (fd d)
      (begin (jumb (/ d 2) (- k 1))
        (end_fill)
        (penup)
        (fd d)
        (pendown)
        (begin_fill))))))

(color "purple")
(jumb 40 2)

;(penup)
(fd 40)
(lt 90)
(forward 60)
(pendown)

(color "orange")
(jumb 40 2)

;(penup)
(fd 40)
(lt 90)
(forward 60)
(pendown)

(color "yellow")
(jumb 40 2)

;(penup)
(forward 60)
(pendown)


(color "black")
(jumb 40 2)

;(penup)
(forward 60)
(pendown)


(color "red")
(jumb 40 2)

;(penup)
(forward 60)
(pendown)


(color "gray")
(jumb 40 2)


;(penup)
(fd 40)
(pendown)

(color "purple")
(jumb 40 2)

;(penup)
(fd 40)
(lt 90)
(forward 60)
(pendown)

(color "orange")
(jumb 40 2)

;(penup)
(fd 40)
(lt 90)
(forward 60)
(pendown)

(color "yellow")
(jumb 40 2)

;(penup)
(forward 60)
(pendown)


(color "black")
(jumb 40 2)

;(penup)
(forward 60)
(pendown)


(color "red")
(jumb 40 2)

;(penup)
(forward 60)
(pendown)


(color "gray")
(jumb 40 2)