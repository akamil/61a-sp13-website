;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: Happy Dance
;;;
;;; Description:
;;;     Lotus flowers and
;;;     Snails doing the
;;;     Happy dance no frogs
;;;

(speed 9999999)
(forward 1000)
(left 90)
(begin_fill)
(color "#000000")
(circle 1000)
(end_fill)

(define (draw)
  (speed 0) 
  (pu) (setposition -250 -100) (color "green") (pd)
  (lily-pad 10 8)
  (pu) (goto -600 500) (pd)
  (lily-pad 15 8)
  (pu) (goto 500 200) (pd)
  (lily-pad 12 8)
  (pu) (goto -500 350) (color "blue") (pd)
  (bubbles 5 8)
  (pu) (goto 200 200) (pd)
  (bubbles 4 8)
  (pu) (goto -500 -400) (pd)
  (bubbles 6 8)
  (pu) (goto -400 300) (color "white") (pd)
  (petal 3)
  (pu) (goto -50 25) (pd)
  (petal 3)
  (pu) (goto 375 0) (pd)
  (petal 3)
  (pu) (goto -600 300) (color "gold") (pd)
  (snail 40 1 30))

(define (repeat k fn)
  (if (> k 0)
    (begin (fn) (repeat (- k 1) fn))
    'done))

(define (arcr size)
  (repeat size (lambda () (fd 1) (rt 1))))

(define (petal n)
  (repeat n (lambda () (repeat 6 (lambda () (arcr 60) (rt 120) (arcr 60) (rt 120) (rt 20))) (pu) (fd 100) (rt 20) (fd 100) (lt 20) (pd))))

(define (polygon side size)
  (repeat 10 (lambda () (repeat side (lambda () (fd size) (right (/ 360 side)))) (pu) (fd (* size 10)) (pd) (rt 25))))

(define (lily-pad n k)
  (if (> k 2)
    (begin (polygon k n) (lily-pad n (- k 1)))))

(define (bubbles n k)
  (if (> k 2)
    (begin (polygon k n) (bubbles (* n 0.8) (- k 1)))))

(define (snail n-sides side angle)
  (cond ((= n-sides 0) 0)
        (else (fd side)
              (right angle)
              (snail (- n-sides 1) (+ side 1) angle))))

(hideturtle)
; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
