;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: Amir puts the "CS" in OlympiCS
;;;
;;; Description:
;;;   <Amir won gold at
;;;    the Olympics now he is
;;;    a Kamillionaire>

(define (draw)
    ; Recursively makes n number of circles displaced slightly overlapping
    (define (ringmaker n t s)
        (if (<= n t) (begin (circle s) (rt 15) (bk 15) (ringmaker (+ n 1) t s))))
    
    ; Function that changes pen color and pen location
    (define (pen-changer c x y)
        (begin (pu) (setpos x y) (color c) (pd)))
    
    ; Draws the rings at a certain location in a certian color
    (define (imagemaker x y c)
        (pen-changer c x y)
        (ringmaker 0 35 55))
    
    ; Draws Amir's eyes
    (define (eyemaker c x y)
        (pen-changer c x y)
        (circle 2)
        (circle 4))
    
    ; Creates circles which is the basis for Amir's body
    (define (circle-filler c x y r)
        (pen-changer c x y)
        (begin_fill)
        (circle r)
        (end_fill))
    
    ; Calls to the Olympic rings
    (imagemaker -75 150 "#0066CC")
    (imagemaker -125 120 "#FFFF00")
    (imagemaker 50 150 "#000000")
    (imagemaker 0 120 "#009900")
    (imagemaker 175 150 "#FF0000")

    ;Calls to Amir's body
    (circle-filler "#1B77E0" -120 -140 100)
    (pen-changer "#FF0000" -25 -165)
    (begin (rt 155) (fd 120) (rt -155))
    (pen-changer "#FF0000" -15 -165)
    (begin (rt -155) (fd 120) (rt 155))
    (pen-changer "#FF9933" -75 -25)
    
    ; Recursively Drawing the Hair
    (begin_fill)
    (ringmaker 0 23 0)
    (end_fill)
    (color "#000000")
    (ringmaker 0 11 5)
    
    ; Eyes
    (eyemaker "#000000" -35 -25)
    (eyemaker "#000000" 15 -25)
    
    ; Facial Features
    (pen-changer "#FF3333" 10 -65)
    (begin (begin_fill) (rt 30) (circle 30 -120) (rt 90) (end_fill))
    (circle-filler "#FFCC00" -30 -125 40)
    (circle-filler "#FF9933" -140 -125 20)
    (pen-changer "#1877E0" 115 -43)
    
    ; Arm
    (begin (begin_fill)(rt -15) (fd 100) (rt 90) (fd 15) (rt 90) (fd 100) (lt 215) (end_fill))
    
    ; Gold Medal and Shoes
    (circle-filler "#FF9933" 100 -25 20)
    (circle-filler "#000000" -85 -242 20)
    (circle-filler "#000000" 17 -238 20)

    (ht))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
