;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: Starry Night (Berkeley Edition)
;;;
;;; Description:
;;;   Warm spring nights under
;;;   a recursive Milky Way;
;;;   we code until dawn

 

(define (draw)
  ; *YOUR CODE HERE*
  (define (abs num)
  (if (> num 0)
    num)
    (- 0 num))
  (begin
  (window-size 800 494)
  
  (speed 0)
  (pu)
  (color "#0000CD")

  (define (bg-line pos dir) ;draws background to simulate Van Gogh's brushstrokes
    (if (> pos -200)
      (begin
        (pu)
        (if (= 0 (modulo dir 2))
          (begin 
            (goto -400 pos)
            (seth 90)
            (rt 90)
            (lt 90))
          (begin
            (seth 180)
            (fd 25)
            (seth 270)))
        
        (pd)
      (define (draw-bg iter)
        (if (> iter 0)
          (begin
            (fd 15)
            (rt 45)
            (fd 15) 
            (lt 45)
            (fd 15)
            (lt 45)
            (fd 15) 
            (rt 45)
            (draw-bg (- iter 1)))))
      (draw-bg 16) 
    (bg-line (- pos 10) (+ 1 dir)))))
  (goto -400 300)
  (begin_fill)
  (bg-line 300 2)
  (goto -400 -100)
  (end_fill)
  (set_bg_color "#0000EB")

  (color "orange")

  (define (draw-star len dir)
  	(if (> len 2)
  		(begin
        (if (= dir 1)
  		(rt 92) (lt 92))
  		(fd len)
  		(draw-star (- len 1) dir))
  		))

  (pu)
  (goto 250 260) 
  (seth 45)
  (pd)
  (draw-star 100 1) 
  (pu)
  (goto 250 260) 
  (lt 105) 
  (pd)
  (draw-star 100 2) 
  (pu)

  (goto 50 -40)
  (color "#ffcc33")
  
  (seth 45)
  (pd)
  (draw-star 50 1)

  (pu)
  (goto 50 -40)
  (rt 166) 
  (pd)
  (draw-star 50 2) 
  (pu)
  

  (goto -270 270)
  (seth 45)
  (begin_fill)
  (pd)
  (draw-star 75 1)  
  (end_fill)
  (pu)

  (goto -250 150)
  (define (star2 len dir)
    (fd 100)
    (rt 90)
    (fd 100)
    (rt 90)
    (fd 100)
    (rt 90)
    (fd 100)
    (rt 90)
    (fd 50)
    (lt 90)
    (fd 25)
    (rt 90)
    (fd 100)
    (rt 90)
    (fd 100)
    (rt 90)
    (fd 100)
    (rt 90)
    (fd 100)
    (rt 90)
    )


(define (drawer lst) ; for dragon fractal
  (if (> (length lst) 0)
    (begin
      (pu)
    (if (= (car lst) 1) 
      (begin 
        (color "orange")
        (lt 90)
        (fd 7))
      (begin 
        (color "yellow")
        (rt 90)
        (fd 7)))
    (pd)
    (fd 1)
    (drawer (cdr lst)))))


(define (change-middle lst)
  (define middle (/ (+ 1 (length lst)) 2))
  (define (helper lst k)
    (if (= k 1)
        (cons (if (= 1 (car lst)) 0 1) (cdr lst))
        (cons (car lst) (helper (cdr lst) (- k 1)))))
  (helper lst middle)
  )

(define (dragon)
  (define (create-list iter lst)
    (if (> iter 0)
      (begin
        (define newlist (append (append lst (cons 1 nil)) (change-middle lst)))
        (create-list (- iter 1) newlist))
    lst))
  (define thelist (create-list 8 (cons 1 nil)))

  (drawer thelist))
  (pu)
  (goto -150 150)
  (seth 150)
  (pd)
  (dragon) 

  (pu)
  (lt 90)
  (goto 80 5)
  (pd)
  (dragon) 
  (pu)
  (goto 250 200)
  (pd)
  (begin_fill)


  (define (green-lookup iter)
    (define itermod (modulo iter 2))
    (cond ((= itermod 1) "#006600") 
      ((= itermod 0) "#006633"))) 
  (define (draw-hills iter)
    (if (> iter 0)
      (begin 
        (lt 90)
        (fd 40)
        (rt 90)
        (color (green-lookup iter))
        (begin_fill)
        (circle (if (= 0 (modulo iter 2)) 40 50))
        (end_fill)
        (draw-hills (- iter 1)))))
  (pu)
  (goto 450 -200)
  (seth 0)
  (pd)
  (draw-hills 20)
  (begin_fill)
  (color "#006600")
  (goto -420 -200)
  (seth 90)
  (fd 820)
  (rt 90)
  (fd 150)
  (rt 90)
  (fd 820)
  (rt 90)
  (fd 150)
  (end_fill)

  (define (mini-star size iter)
    (if (> iter 0)
    (begin
    (rt 30)
    (pd)
    (fd size)
    (bk (* size 2))
    (fd size)
    (mini-star size (- iter 1)))))
  (pu)

  (define (draw-minis iter)
    (color "#FFFF66")
    (if (> iter 0)
      (begin
    (pu)
    (seth 90)
    (fd 100)
    (rt 90)
    (fd 75)
    (pd)
    (mini-star 25 12)
    (draw-minis (- iter 1)))))
  (goto -530 180)
  (draw-minis 4)
  (pu)
  (goto 100 150)
  (draw-minis 3)
  (pu)


  (define (campanile)
    (define (windows iter)
      (color "yellow")
      (if (> iter 0)
        (begin
      (goto (+ -310 (* iter 30)) -150)
      (seth 0)
      (pd)
      (begin_fill)
      (fd 80)
      (rt 90)
      (fd 20)
      (rt 90)
      (fd 80)
      (rt 90)
      (fd 20)
      (end_fill)
      (pu)
      (windows (- iter 1)))))
    
    (goto -290 -300)
    (seth 0)
    (begin_fill)
    (pd)
    (fd 325)
    (rt 90)
    (fd 10)
    (rt 90)
    (fd 45)
    (lt 180)
    (fd 35)
    (rt 15)
    (fd 150)
    (rt 150)
    (fd 150)
    (rt 15)
    (fd 35)
    (rt 180)
    (fd 45)
    (rt 90)
    (fd 10)
    (rt 90)
    (fd 325)
    (rt 90)
    (fd 110)
    (end_fill)
    (windows 3)
    )
  (pu)
  (color "#666666")
  
  (campanile)
  
 
  (ht)))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
