;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: Roots
;;;
;;; Description:
;;;   Roots lacking color.
;;;   But, with some recursion, will
;;;   grow colorfully.

;;; *YOUR CODE HERE*
(define (tree d r a w)
        (cond ((> w 0)
	       (fd d) (left r)
	       (tree (* d a) r a (- w 1))
	       (right 100)
	       (tree (* d a) r a (- w 1))
	       (left 100)(right r)(right 180)(fd d)(left 180))
	       (else fd 2)
        ))

(define (draw)
        (penup)(color 'turquoise)(goto 0 130)(pendown)
        (speed 0)
        (right 180)(fd 60)(right 180)
        (tree 60 45 .75 10)
        (penup)(color 'forestgreen)(pendown)
        (speed 0)
        (right 180)(fd 60)(right 180)
        (tree 60 45 .75 10)
        (penup)(color 'springgreen2)(pendown)
        (speed 0)
        (right 180)(fd 60)(right 180)
        (tree 60 45 .75 10)
        (penup)(color 'sienna)(pendown)
        (speed 0)
        (right 0) (fd 20) (right 180)
        (tree 60 45 .75 7)
        )

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
