;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: <Your title here>
;;;
;;; Description:
;;;   I was so convinced
;;;   this was the answer to twenty-two.
;;;   You could say I was pretty close.

(define (draw)
	(speed 0)
	(hax 80 4))

(define (hax d k)
  ; *** YOUR CODE HERE ***
  (hex (lambda () 
      (if (= k 1) (fd d) (side d k)))))

(define (side d k)
  (hax (/ d 2) (- k 1))
  (penup)
  (fd (* d 1.732))
  (pendown))

(define (hex fn)
  (repeat 6 (lambda () (fn) (lt 60))))

(define (repeat k fn)
    (if (> k 0)
    (begin (fn) (repeat (- k 1) fn))
    nil))

(hideturtle)
; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
