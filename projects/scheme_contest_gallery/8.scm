;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: Mutant Snowflake
;;;
;;; Description:
;;;   Koch and Bentley say,
;;;    "Snowflakes have six sides apiece,"
;;;    but I disagree.

(define (draw)
  (speed 0)
  (goto -50 150)
  (seth 141)
  (clear)
  (edge 300 6)
  (right 144)
  (edge 300 6)
  (right 144)
  (edge (* 300 const) 5)
  (left 72)
  (edge (* 300 const) 5)
  (right 144)
  (ht))

(define (edge d k)
  (if (<= k 0)
      (forward d)
      (let ((piece (lambda () (edge (* d const)
                                   (- k 1)))))
        (piece)
        (left 72)
        (piece)
        (right 144)
        (piece)
        (left 72)
        (piece))))

(define (phi-converge x)
  (if (<= x 0)
      1
      (+ 1 (/ 1 (phi-converge (- x 1))))))

(define const (- 2 (phi-converge 30)))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
