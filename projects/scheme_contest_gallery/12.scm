;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: finals more like fml amirite
;;;
;;; Description:
;;;    Like this arrow, our
;;;    grades after finals will point
;;;    anywhere but up.

(define calcolors
    '((0.06 0.3 0.57) (0.72 0.53 0.15)))

(define (draw)
  (color_rgb (car calcolors))
  (bgcolor (cdr calcolors))
  (basecase 1 3 2 1)
  )

(define (basecase sl s l a)
  (if (> sl 480) 'okay (changingpoly sl s l a))
  )

(define (changingpoly sidelen sides lenchange angchange)
  (fd sidelen)
  (lt (+ (/ 360 sides) angchange))
  (basecase (+ sidelen lenchange) sides lenchange (* 0.99 angchange))
  )

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
