;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: Spacetime Shell
;;;
;;; Description:
;;;   <I watch turtles weave
;;;    With their universal thread
;;;    Abstraction is art>

(define (draw)
  (speed 0)
  (background 1)
  (surface 3.14159265359 6.28318530718)
  (wave 0 401)
  (surface 0 3.14159265359)
  (wave -400 1)
  (hideturtle))

(define (xy_t func_x func_y start stop step angle)
	(define (getpt time)
	    (let ((x (- (* (func_x time) (cos angle)) (* (func_y time) (sin angle))))
		      (y (+ (* (func_x time) (sin angle)) (* (func_y time) (cos angle)))))
	    (cons x y)))
	(penup)
	(goto (car (getpt start)) (cdr (getpt start)))
	(pendown)
	(define (plothelper para)
		(goto (car (getpt para)) (cdr (getpt para)))
		(if (< (+ para step) stop)
			(plothelper (+ para step))))
	(plothelper (+ start step)))

(define (background n)
		(let ((a n) (b n)
			  (bodyx '(- (* a a) (* b b))))
		(color 'blue)
		(xy_t (lambda (a) (eval bodyx)) (lambda (a) (* 2 a b)) -20 21 1 0)
		(color 'purple)
		(xy_t (lambda (b) (eval bodyx)) (lambda (b) (* 2 a b)) -20 21 1 0))
		(if (< n 20)
			(background (+ n 1))))

(define (surface startangle stopangle)
	(define (func_x para)
		(* 100 (sin para)))
	(define (func_y para)
		(* 200 (cos para)))
	(define (surface_helper angle greenlevel bluelevel)
		(color3 0 greenlevel bluelevel)
		(xy_t func_x func_y startangle stopangle 0.1 angle)
		(if (< angle 1.5707963267)
			(surface_helper (+ angle 0.01256637062) (- greenlevel 0.008) (+ bluelevel 0.008))))
	(pensize 3)
	(surface_helper 0 1 0)
	(pensize 1))

(define (wave start stop)
	(color 'red)
	(define (wavehelper y)
		(xy_t (lambda (c) c) (lambda (x) (+ (* 10 (sin (/ x 30))) y)) start stop 5 0)
		(if (< y 10)
			(wavehelper (+ y 2))))
	(wavehelper -10))

(draw)