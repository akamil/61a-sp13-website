;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: <Me Gusta>
;;;
;;; Description:
;;;   <That moment when your
;;;    code runs just right the first time: 
;;;    biggest Me Gusta.>

(define (draw)
(penup)
(clear)
(setposition 460 -8)
(setheading 0)
(color "DarkSlateBlue")
(pendown)
(begin_fill)
(circle 600) ;;; Blue BG
(end_fill)

(penup)
(setheading 0)
(setposition 180 -8)
(color "Gold")
(pendown)
(begin_fill)
(circle 300) ;;; Yellow BG
(end_fill)

(penup)
(setposition 40 -8)
(setheading 0)
(color "White")
(pendown)
(begin_fill)
(circle 150) ;;; face
(end_fill)

(penup)
(color "Black")
(setposition -130 0)
(pendown)
(circle 30) ;;; left eyeball
(penup)
(setposition -127 -8) 
(setheading 180)
(pendown)
(circle -32 170) ;;; left eyebags
(penup)
(setposition -125 -10)
(setheading 180)
(pendown)
(circle -35 160)

(penup)
(setheading 0)
(setposition -165 0)
(pendown)
(circle 5) ;;; left pupil
(penup)
(setposition -5 -2)
(pendown)
(circle 30) ;;; right eyeball
(penup)
(setposition -66 -10)
(pendown)
(setheading 180)
(circle 30 175)
(penup)
(setposition -30 -45)
(setheading 90)
(pendown)
(circle 31 105)
(penup)
(setheading 0)
(setposition -10 -2)
(pendown)
(circle 5) ;;; right pupil
(penup)
(setposition -160 50)
(pendown)
(right 90)
(forward 140) ;;; long forehead line
(penup)
(setposition -120 40) ;;; short forehead line
(pendown)
(forward 65)
(penup)
(setposition -140 37)
(pendown)
(forward 20)
(penup)
(setposition -80 37)
(pendown)
(forward 20)
(penup)
(setposition -195 30)
(pendown)
(forward 75) ;;; left eyelid
(penup)
(setposition -70 28)
(pendown)
(forward 75) ;;; right eyelid
(penup)
(setposition -82 -47)
(pendown)
(setheading 120)
(forward 10)
(setheading 150)
(circle 15 250) ;;; right nostril

(penup)
(setheading 180)
(setposition -83 10) 
(pendown)
(forward 58) ;;; nose bridge
(circle -20 230) ;;; left nose lobe
(penup)

(penup)
(setheading 72)
(setposition -200 -92) 
(pendown)
(circle -300 36) ;;; main mouth arc
(penup)
(setposition -140 -90)
(pendown)
(setheading 72)
(circle -140 30)
(penup)

(setheading 30)
(setposition -115 -105)
(pendown)
(circle -20 150)
(penup)
(setposition -125 -110)
(setheading 8)
(pendown)
(circle -28 200) ;;; underchins

(penup)
(setheading -33)
(setposition -215 -96) 
(pendown)
(circle -25 150) ;;; left mouth
(penup)


(penup)
(setheading 60)
(setposition -20 -70)
(pendown)
(circle -20 120) ;;; right mouth
)

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)
