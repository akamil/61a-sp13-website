;;; Scheme Recursive Art Contest Entry
;;;
;;; Please do not include your name or personal info in this file.
;;;
;;; Title: rockets
;;;
;;; Description:
;;;   rockets rockets rock
;;;   ets rockets rockets rockets
;;;   rockets rockets rock



   
(define UNIT_LEN 50)
(define EPSILON 1)
(define DF   0.8)
(define SPACE 0.1)
(define PI  3.14159265359)
(define NUM-STEPS 5)

(define (psi-calc k)
 (arctan    (/ (* .4 (- 1 (pow DF (+ k 1))))
               (+ 1 (- DF) (- (pow DF k)) (pow DF (+ k 1))))))

(define PSI (psi-calc NUM-STEPS))
(define PSID (* 360 (/ PSI (* 2 PI))))

(define (sind phi)
  (sin (* (/ phi 360) 2 PI)))

(define (cosd phi)
  (cos (* (/ phi 360) 2 PI)))

(define (tand phi)
  (/ (sind phi) (cosd phi)))

(define (gsum r k)
  (/ ( - 1 (pow r (+ k 1))) (- 1 r)))

(define (top-length b k)
   (* (/ b 2) (- 1 (pow DF k))))

(define (side-length b k)
   (/ (top-length b k) (cosd PSID)))

(define (jump x y)
   (begin 
     (penup)
     (goto x y)
     (pendown)
    ))



(define (jump-mag-rel len phi)
   (begin
    (penup)
    (right phi)
    (forward len))
    (pendown))

(define (jump-mag-abs len phi)
   (begin 
     (penup)
     (setheading phi)
     (forward len)
     (pendown)))

(define (set x y phi)
  (jump x y)
  (setheading phi))

(define (reset)
   (jump 0 0)
   (setheading 0))



(define (roc phi unit ?reset)
  (if ?reset
    (reset)
    (jump-mag-abs (* SPACE unit) (+ phi 90))))


(define (branch len)
    (forward len)
    (backward len))

(define (R x y phi size ?reset) ;angle measured between vert and line you're writing on     size =height of R, first is 200
   (begin
      (if ?reset   
      (set x y phi)
      (setheading phi))
      (forward size)                                    ;assume that turtle is facing along vertical of R
      (right 90)
      (forward (/ size 4))
      (circle ( - (/ size 4)) 180)
      (forward (/ size 4))
      (left 135)
      (forward ( * 1.41 (/ size 2)))
      (roc phi size ?reset)))
          

(define (O x y phi diam   ?reset)
  (begin
     (if ?reset
     (set x y (+ phi 90))
     (begin 
       (jump-mag-rel (/ diam 2) 0)
       (setheading (+ phi 90))))
     (circle (/ diam 2))
     (jump-mag-rel (/ diam 2) 0)
     (roc phi diam  ?reset)))

(define (C x y phi diam   ?reset)
   (begin
     (if ?reset
     (set x y (- phi 90))
     (begin 
        (jump-mag-rel (/ diam 2) 0)
        (setheading (- phi 90))))
     (circle (- (/ diam 2 )) 180)
     (jump-mag-rel diam 90)
     (left 90)
     (roc phi diam  ?reset)))

(define (K x y phi height ?reset)
  (begin
    (if ?reset
    (set x y phi)
    (setheading phi))
    (forward height)
    (backward (/ height 2))
    (right 45)
    (branch (* (/ height 2) 1.412))
    (right 90)
    (forward (* (/ height 2) 1.412))
    (roc  phi height  ?reset)))

(define (E x y phi height ?reset)
  (begin
    (if ?reset
    (set x y phi)
    (setheading phi))
    (forward height)
    (right 90)
    (branch (/ height 2))
    (right 90)
    (forward (/ height 2))
    (left 90)
    (branch (/ height 2))
    (right 90)
    (forward (/ height 2))
    (left 90)
    (forward (/ height 2))
    (roc phi height  ?reset)))


(define (T x y phi height ?reset)
  (begin 
    (if ?reset
    (set x y phi)
    (begin
      (jump-mag-rel (/ height 2) 0)   
      (setheading phi)))
    (forward height)
    (left 90)
    (branch (/ height 2))
    (right 180)
    (branch (/ height 2))
    (right 90)
    (forward height)
    (left 90)
    (jump-mag-rel (/ height 2) 0)
    (roc phi height  ?reset)))

(define (S x y phi height ?reset) 
   (begin
     (if ?reset
     (set x y (- phi 90))
     (begin 
        (jump-mag-rel (/ height 4) 0)
         (setheading (- phi 90))))
     (branch ( / height 4))
     (circle (-(/ height 4)) -180)
     (circle  (/ height 4 ) -180)
     (left 180)
     (branch (/ height 4))
     (left 180)
     (jump-mag-rel height 270)
     (left 90)
     (roc phi height  ?reset)))


(define ROCKETS (list R O C K E T S))


(define (spell lst x y phi unit)

  (define (subs-spell lst x y phi unit ?first)
    (cond ((null? lst) (reset))
          ( ?first (begin
                     (set x y phi)
                     ((car lst) x y phi unit #f)
                     (subs-spell (cdr lst) x y phi unit #f)))
          (else
             ((car lst) x y phi unit #f)
              (subs-spell (cdr lst) x y phi unit #f))))

  (subs-spell ROCKETS x y phi unit #t))

(define (spell-rockets-vert x y phi unit)
   (spell ROCKETS x y (- phi 90) unit))



(define (abs a)
   (if (< a 0)
       (- a)
       a))

(define (approx a b)
  (< (abs (- a b)) EPSILON))

(define (body x y phi unit)
   (spell ROCKETS x y (- phi 90) unit))

(define (nose-cone x y phi unit)
    (decay x y phi unit #f))

(define (inv-cone x y phi unit)
    (decay x y (+ 180 phi) unit 0 ))

(define (prop x y phi prop-unit)
   (let ((theta (- phi 180)))
   (sub-prop x y (+ theta 40) prop-unit (- theta 40) phi )))
   
(define (sub-prop x y theta sub-width last-theta phi)
    (if (< theta last-theta)
        (reset)
        (begin
         (let ((dx (* 0.2  sub-width (cosd phi)))
               (dy (- (* 0.2 sub-width (sind phi)))))
          (spell-rockets-vert x y theta sub-width) 
          (sub-prop (+ x dx) (+ y dy) (- theta 20) sub-width last-theta phi)))))
        
    


(define (decay x y phi unit ?stop-index)              
  (let ((theta (- 90 phi)))      
  (let ( (dx (+ (*  unit (cosd theta))  (*  (sind theta) (* 2.25 (- 1 DF) unit))))
         (dy (- (*  unit (sind theta))  (* (cosd theta) (* 2.25 (- 1 DF) unit)))))
   (cond ((and ?stop-index (eq? ?stop-index (+ NUM-STEPS 1))) (reset))
        ( (< unit 1) (reset))
        (else
         (begin
          (if ?stop-index
              (define next-index (+ ?stop-index 1))
              (define next-index #f))
          (spell ROCKETS x y phi unit)
           (decay (+ x dx) (+ y dy) phi ( * DF unit) next-index)))))))
           

(define (rocket x y phi body-width)
   (begin
    (let (( l  (+ (* 4.5 body-width) (* 6 ( * SPACE body-width))))
          ( theta (- 90 phi))
          ( s (side-length body-width NUM-STEPS))
          ( delta (- PSID ( - 90 phi))))
    (let (( dx-nose (- (* l (cosd theta)) (* body-width (sind theta))))
          ( dy-nose (+ (* l (sind theta)) (* body-width (cosd theta))))
          ( dx-prop (- (+ (* body-width (sind theta)) (* s (sind delta)))))
          ( dy-prop (- (* body-width (cosd theta)) (* s (cosd delta))))
          ( cap-unit (/ body-width 5)))
    (let (( prop-unit (* cap-unit (pow DF NUM-STEPS)))) 
    (begin
    (body x y phi body-width) 
    (nose-cone (+ x dx-nose) (+ y dy-nose) phi  cap-unit)
    (inv-cone  x y phi cap-unit)
    (prop (+ x dx-prop) (+ y dy-prop) phi cap-unit)))))))



 
(define (draw)
  (pendown)
  (speed 0)

  ; *YOUR CODE HERE*
  (rocket -150 -125 45 85))

; Please leave this last line alone.  You may add additional procedures above
; this line.  All Scheme tokens in this file (including the one below) count
; toward the token limit.
(draw)



