
===========================
Scheme Recursion Exhibition
===========================

Please vote for your favorite entry in this semester's 61A Recursion Exposition
contest. The winner should exemplify the principles of elegance, beauty, and
abstraction that are prized in the Berkeley computer science curriculum. As an
academic community, we should strive to recognize and reward merit and
achievement (translation: please don't just vote for your friends).

In order to vote and earn 1 of the 2 points for HW14, complete and submit
`hw14.py <../../hw/hw14.py>`_. Voting is due on Friday, May 10, 11:59pm.

.. contents:: :depth: 2

----


Featherweight Division
======================


Submission 0 - Your title here: a masterpiece of modern art
-----------------------------------------------------------

| Zeno was a wise man
| Ramanajun was wiser
| Learn to add, man


.. image:: 0.png
   :height: 600px



`0.scm <0.scm>`_ (52 tokens)



Submission 1
------------

| Haikus are easy
| But sometimes they don't make sense
| Refrigerator


.. image:: 1.png
   :height: 600px



`1.scm <1.scm>`_ (221 tokens)



Submission 2
------------


.. image:: 2.png
   :height: 600px



`2.scm <2.scm>`_ (238 tokens)


Submission 3 - Happy Dance
--------------------------

| Lotus flowers and
| Snails doing the
| Happy dance no frogs


.. image:: 3.png
   :height: 600px



`3.scm <3.scm>`_ (242 tokens)


Submission 4 - Spacetime Shell
------------------------------

| I watch turtles weave
| With their universal thread
| Abstraction is art


.. image:: 4.png
   :height: 600px



`4.scm <4.scm>`_ (243 tokens)


Submission 5 - Our Grades in this Class
---------------------------------------

| I am really f***ed
| Where did the semester go
| F*** F*** F*** F*** F***


.. image:: 5.png
   :height: 600px



`5.scm <5.scm>`_ (31 tokens)

Submission 6 - Me Gusta
-----------------------

| That moment when your
| code runs just right the first time:
| biggest Me Gusta.


.. image:: 6.png
   :height: 600px



`6.scm <6.scm>`_ (249 tokens)


Submission 7 - Amir puts the "CS" in OlympiCS
---------------------------------------------

| Amir won gold at
| the Olympics now he is
| a Kamillionaire


.. image:: 7.png
   :height: 600px



`7.scm <7.scm>`_ (210 tokens)


Submission 8 - Mutant Snowflake
-------------------------------

| Koch and Brentley say,
| "Snowflakes have six sides apiece,"
| but I disagree.


.. image:: 8.png
   :height: 600px



`8.scm <8.scm>`_ (88 tokens)


Submission 9 - The Spinning Stars and the Stripes Forever
---------------------------------------------------------

| Don't look for too long.
| You may see the stars spinning
| Go America!


.. image:: 9.png
   :height: 600px


`9.scm <9.scm>`_ (251 tokens)


Submission 10 - Roots
---------------------

| Roots lacking color.
| But with some recursion, will
| grow colorfully.


.. image:: 10.png
   :height: 600px


`10.scm <10.scm>`_ (121 tokens)


Submission 11
-------------

| I was so convinced
| this was the answer to twenty-two
| You could say I was pretty close


.. image:: 11.png
   :height: 600px


`11.scm <11.scm>`_ (66 tokens)


Submission 12 - finals more like fml amirite
--------------------------------------------

| Like this arrow, our
| grades after the finals will point
| anywhere but up


.. image:: 12.png
   :height: 600px


`12.scm <12.scm>`_ (61 tokens)


Submission 13 - Abstract Tower
------------------------------

| Recursive iteration, loop formation.
| Identical symmetry, mirrors perfectly.


.. image:: 13.png
   :height: 600px


`13.scm <13.scm>`_ (166 tokens)


Submission 14 - And Rain Will Make The Flowers Grow
---------------------------------------------------

| Bursting into bloom
| A bouquet of gracideas
| Gift of recursion.


.. image:: 14.png
   :height: 600px


`14.scm <14.scm>`_ (139 tokens)


Submission 15 - recursion springs eternal
-----------------------------------------

| Recursive flowers
| drawn in an infinite loop--
| Eternal spring


.. image:: 15.png
   :height: 600px


`15.scm <15.scm>`_ (95 tokens)


----

Heavyweight Division
====================


Submission 18 - Starry Night (Berkeley Edition)
-----------------------------------------------

| Warm sprint nights under
| a recursive Milky Way;
| we code until dawn

.. image:: 18.png
   :height: 600px


`18.scm <18.scm>`_ (615 tokens)


Submission 19 - Eleisa's Miracle
--------------------------------

| A useless item,
| Don't buy it while playing in 
| The League of Legends

.. image:: 19.png
   :height: 600px


`19.scm <19.scm>`_ (276 tokens)


Submission 20 - rockets
-----------------------

| rockets rockets rock
| ets rockets rockets rockets
| rockets rockets rock

.. image:: 20.png
   :height: 600px


`20.scm <20.scm>`_ (950 tokens)


Submission 21 - now in 3D (oh god I haven't slept)
--------------------------------------------------

| Invest in this code
| Get your money back and more
| A Pyramid Scheme

.. image:: 21.png
   :height: 600px


`21.scm <21.scm>`_ (400 tokens)


Submission 22 - Thanks Amir
---------------------------

| Amir was awesome
| But now the year is over
| Amir is awesome.

.. image:: 22.png
   :height: 600px


`22.scm <22.scm>`_ (504 tokens)


Submission 23 - Majoras Mask
----------------------------

| Little Goblin Here
| Now Wearing Majoras Mask
| Creating Chaos

.. image:: 23.png
   :height: 600px


`23.scm <23.scm>`_ (1059 tokens)
