# Name:
# Login:
# TA:
# Section:
# Q1.

def stone_age():
    """Returns the year in which Amir took CS 61a."""
    return 2001

# Q2.

def favorite_player():
    """Returns Amir's favorite baseball player."""
    return 'Sergio Romo'

# Q3.

def favorite_number():
    """Returns Amir's favorite number."""
    return 49

