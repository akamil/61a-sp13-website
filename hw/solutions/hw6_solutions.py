# Name:
# Login:
# TA:
# Section:
# Q1.

def divide_by_fact(dividend, n):
    """Recursively divide dividend by the factorial of n.

    >>> divide_by_fact(120, 4)
    5.0
    """
    if n == 0:
        return dividend
    return divide_by_fact(dividend / n, n - 1)

# Q2.

def group(seq):
    """Divide a sequence of at least 12 elements into groups of 4 or
    5. Groups of 5 will be at the end. Returns a tuple of sequences, each
    corresponding to a group.

    >>> group(range(14))
    (range(0, 4), range(4, 9), range(9, 14))
    >>> group(tuple(range(17)))
    ((0, 1, 2, 3), (4, 5, 6, 7), (8, 9, 10, 11), (12, 13, 14, 15, 16))
    """
    num = len(seq)
    assert num >= 12
    if num == 12 or num == 13:
        return seq[:4], seq[4:8], seq[8:]
    elif num == 14:
        return seq[:4], seq[4:9], seq[9:]
    elif num == 15:
        return seq[:5], seq[5:10], seq[10:]
    return (seq[:4],) + group(seq[4:])


"""

   ====
    ==
========== <--- spatula underneath this crust
 ========

    ||
    ||
   \||/
    \/

========== }
    ==     } flipped
   ====    }
 ========

"""

# Q3.

def partial_reverse(lst, start):
    """Reverse part of a list in-place, starting with start up to the end of
    the list.

    >>> a = [1, 2, 3, 4, 5, 6, 7]
    >>> partial_reverse(a, 2)
    >>> a
    [1, 2, 7, 6, 5, 4, 3]
    >>> partial_reverse(a, 5)
    >>> a
    [1, 2, 7, 6, 5, 3, 4]
    """
    end = len(lst) - 1
    while start < end:
        lst[start], lst[end] = lst[end], lst[start]
        start, end = start + 1, end - 1

# Q4.

def index_largest(seq):
    """Return the index of the largest element in the sequence.

    >>> index_largest([8, 5, 7, 3 ,1])
    0
    >>> index_largest((4, 3, 7, 2, 1))
    2
    """
    assert len(seq) > 0
    return seq.index(max(seq))

# Alternate solution
def index_largest2(seq):
    """Return the index of the largest element in the sequence.

    >>> index_largest2([8, 5, 7, 3 ,1])
    0
    >>> index_largest2((4, 3, 7, 2, 1))
    2
    """
    assert len(seq) > 0
    largest_index, largest_val = 0, seq[0]
    for i in range(1, len(seq)):
        if seq[i] > largest_val:
            largest_index, largest_val = i, seq[i]
    return largest_index

# Q5.

def pizza_sort(lst):
    """Perform an in-place pizza sort on the given list, resulting in
    elements in descending order.

    >>> a = [8, 5, 7, 3, 1, 9, 2]
    >>> pizza_sort(a)
    >>> a
    [9, 8, 7, 5, 3, 2, 1]
    """
    pizza_sort_helper(lst, 0)

def pizza_sort_helper(lst, start):
    if start < len(lst)-1:
        partial_reverse(lst, start + index_largest(lst[start:]))
        partial_reverse(lst, start)
        pizza_sort_helper(lst, start + 1)

# Q6.

def make_accumulator():
    """Return an accumulator function that takes a single numeric argument and
    accumulates that argument into total, then returns total.

    >>> acc = make_accumulator()
    >>> acc(15)
    15
    >>> acc(10)
    25
    >>> acc2 = make_accumulator()
    >>> acc2(7)
    7
    >>> acc3 = acc2
    >>> acc3(6)
    13
    >>> acc2(5)
    18
    >>> acc(4)
    29
    """
    total = [0]
    def accumulator(amount):
        total[0] += amount
        return total[0]
    return accumulator


# Q7.

def make_accumulator_nonlocal():
    """Return an accumulator function that takes a single numeric argument and
    accumulates that argument into total, then returns total.

    >>> acc = make_accumulator_nonlocal()
    >>> acc(15)
    15
    >>> acc(10)
    25
    >>> acc2 = make_accumulator_nonlocal()
    >>> acc2(7)
    7
    >>> acc3 = acc2
    >>> acc3(6)
    13
    >>> acc2(5)
    18
    >>> acc(4)
    29
    """
    total = 0
    def accumulator(amount):
        nonlocal total
        total += amount
        return total
    return accumulator


# Q8.

# Old version
def count_change(a, coins=(50, 25, 10, 5, 1)):
    if a == 0:
        return 1
    elif a < 0 or len(coins) == 0:
        return 0
    return count_change(a, coins[1:]) + count_change(a - coins[0], coins)

# Version 2.0
def make_count_change():
    """Return a function to efficiently count the number of ways to make
    change.

    >>> cc = make_count_change()
    >>> cc(500, (50, 25, 10, 5, 1))
    59576
    """
    computed = {}
    def count_change(a, coins=(50, 25, 10, 5, 1)):
        if a == 0:
            return 1
        elif a < 0 or len(coins) == 0:
            return 0
        elif (a, coins) in computed:
            return computed[(a, coins)]
        count = count_change(a, coins[1:]) + count_change(a - coins[0], coins)
        computed[(a, coins)] = count
        return count
    return count_change

