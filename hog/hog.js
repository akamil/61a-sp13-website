turnTime = 1700;

var newGame = function(goal, ai) {

  var rollDice = function(n, score, sides) {
    var one = false;
    var sum = 0;
    var dice = $("#dice");
    dice.empty();
    for (var i = 0; i < n; i++) {
      var outcome = Math.floor(Math.random() * sides)+1;
      addDice(dice, outcome);
      if (outcome == 1) {
        one = true;
      }
      sum += outcome;
    }
    if (one && score != 49) {
      return 1;
    }
    return sum;
  }

  var addDice = function(dice, outcome) {
    dice.append('<img width="64px" src="die' + outcome + '.png" />');
  }

  var takeTurn = function(n, score, opp, sides) {
    var points = 0;
    if (n == 0) {
      $("#dice").empty();
      points = Math.floor(opp / 10) + 1;
    } else {
      points = rollDice(n, score, sides);
    }
    if (points % 6 == 0) {
      points = points + Math.floor(points / 6);
    }
    return points;
  }

  var takeAction = function(score, opp, n, sides) {
    var points = takeTurn(n, score, opp, sides);
    actions.text("You rolled " + n + " time(s) and scored " + points + " point(s)");

    score += points;
    $('#score').text(score);
    $("#niners").removeClass("red");
    if (score == 49) {
      $("#niners").addClass("red");
    }

    setTimeout(function() {
      if (score < goal) {
        oppAction(score, opp);
      } else {
        actions.text("You win!");
      }
    }, turnTime);
  }

  var oppAction = function(score, opp) {
    var sides = numSides(score, opp);
    var n = Math.min(5, maxDice(score, opp));
    var points = takeTurn(n, opp, score, sides);
    actions.text("Your opponent rolled " + sides + "-sided dice " + n + " time(s) and scored " + points + " point(s)");

    opp += points;
    $('#opp').text(opp);

    setTimeout(function() {
      if (opp < goal) {
        showActions(score, opp);
      } else {
        actions.text("Your opponent won.");
      }
    }, turnTime);
  }

  var maxDice = function(score, opp) {
    var max = 10;
    $("#tied").removeClass("red");
    if ((score + opp) % 10 == 7) {
      max = 1;
      $("#tied").addClass("red");
    }
    return max;
  }

  var numSides = function(score, opp) {
    var sides = 6;
    $("#wild").removeClass("red");
    if ((score + opp) % 7 == 0) {
      sides = 4
      $("#wild").addClass("red");
    }
    return sides;
  }

  // show legal actions in state s
  var showActions = function(score, opp) {
    actions.empty();
    var max = maxDice(score, opp);
    var sides = numSides(score, opp);
    actions.text('How many ' + sides + ' sided dice will you roll? ');

    for (var n = 0; n <= max; n++) {
      actions.append(addAction(score, opp, n, sides, actions));
    }
  }

  var addAction = function(score, opp, n, sides, actions) {
      var action = $('<button>' + n + '</button>')
      action.click(function(e) {
        actions.empty();
        takeAction(score, opp, n, sides);
      })
      return action;
  }

  var actions = $("#actions");
  showActions(0, 0);
}



$(document).ready(function() {
  // Start game buttons
  $(".new").click(function(e) {
    $("#start").hide();
    $("#scoreboard").css('visibility', 'visible');
    newGame(100, e.srcElement.textContent);
  });

  $("#scoreboard").css('visibility', 'visible');
  newGame(100, "");
});
