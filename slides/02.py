# from hw0 import *
# stone_age()

# Print
print('Go 49ers')
print(-2)
print(1, 2, 3)
print(None, None)
print(print(1), print(2))

# Import
from operator import add, sub, mul, truediv, floordiv
5 / 4
5 // 4
8 / 4
8 // 4
truediv(8, 4)
floordiv(8, 4)
add(14, 28)
add(mul(5, 5), mul(12, 12))

from math import sqrt
sqrt(169)

