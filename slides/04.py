# Definitions
from operator import add, mul, floordiv, mod
def square(x):
    return mul(x, x)
square(21)
square(add(2, 5))
square(square(3))

def sum_squares(x, y):
    return add(square(x), square(y))
sum_squares(3, 4)
sum_squares(5, 12)

# Name conflicts
# def square(square):
#     return mul(square, square)
# square(4)
# def square(mul):
#     return mul(mul, mul)
# square(4)

# Multiple return values
def divide_exact(a, b):
    """Return the quotient and remainder from dividing a by b.

    >>> quotient, remainder = exact_divide(2013, 10)
    >>> quotient
    201
    >>> remainder
    3
    """
    return floordiv(a, b), mod(a, b)

# Boolean logic
True and False
1 > 0 and 2 > 1
a = 3
a == 3
a == 4

# Conditionals
def absolute(x):
    if x < 0:
        return -x
    elif x == 0:
        return 0
    else:
        return x

# Default arguments, assertions, loops
def power(a, b=2):
    """Raises a to the power of b, which must be a non-negative integer.

    >>> power(3, 0)
    1
    >>> power(3)
    9
    >>> power(3, 4)
    81
    """
    assert b >= 0 and b == int(b), "Bad exponent!"
    product = 1
    while b > 0:
        product, b = product * a, b - 1
    return product

# Locally defined functions
def sum_of_squares(n):
    """Return the sum of the squares of the first n positive integers"""
    def square(x):
        return mul(x, x)
    total, k = 0, 1
    while k <= n:
        total, k = total + square(k), k + 1
    return total

sum_of_squares(2)
sum_of_squares(3)

# Functions as return values
def make_adder(n):
    """Return a function that adds n to its argument.

    >>> add_three = make_adder(3)
    >>> add_three(4)
    7
    """
    def adder(k):
        return add(n, k)
    return adder

add_three = make_adder(3)
add_four = make_adder(4)
add_three(2)
add_four(2)
