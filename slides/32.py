# Assertions
def assert_false():
    assert False, 'False!'

# Common exceptions
def generate_type_error():
    len(3)

def generate_name_error():
    return x

def generate_key_error():
    {}['x']

def generate_value_error():
    [].index('x')

def generate_runtime_error():
    generate_runtime_error()

# Try statements
def basic_try(x):
    try:
        x = 1 / x
    except ZeroDivisionError as e:
        print('handling a', type(e))
        x = 0
    return x

def multiple_excepts():
    funcs = (generate_type_error,
             generate_name_error,
             generate_key_error,
             generate_value_error,
             generate_runtime_error)
    for f in funcs:
        try:
            f()
        except TypeError as e:
            print('handled a type error:', e)
        except NameError as e:
            print('handled a name error:', e)
        except BaseException as e:
            print('handled a', type(e), e)

def invert(x):
    """Return 1/x

    >>> invert(2)
    Never printed if x is 0
    0.5
    """
    result = 1/x  # Raises a ZeroDivisionError if x is 0
    print('Never printed if x is 0')
    return result

def invert_safe(x):
    """Return 1/x, or the string 'divison by zero' if x is 0.

    >>> invert_safe(2)
    Never printed if x is 0
    0.5
    >>> invert_safe(0)
    'division by zero'
    """
    try:
        return invert(x)
    except ZeroDivisionError as e:
        return str(e)

def nested_trys(x):
    try:
        invert_safe(x)
    except BaseException:
        print('Handled!')

def unhandled_errors(m):
    if m == 0:
        invert_safe(1 / 0)
    elif m == 1:
        inverrrrt_safe(1 / 0)
