from datetime import date, timedelta
import os
import sys

"""
Generates a weekly schedule chart. Expects a symbolic link 'www' in
the current directory to the directory containing index.html.
- Amir Kamil
"""

max_conflicts = 2 # maximum number of conflicting events

class Slot(object):
    def __init__(self, day, start, events=[]):
        self.day = day
        self.start = start
        self.events = []
        for e in events:
            self.add_event(e)
    def __hash__(self):
        return 137 * self.day + int(round(2 * self.start))
    def __eq__(self, other):
        if isinstance(other, Slot):
            return self.day == other.day and self.start == other.start
        else:
            return False
    def add_event(self, event):
        self.events.append(event)
        if len(self.events) > 1:
            for e in self.events:
                e.columns = 1
    def render(self, indent_level):
        msg = ''
        cols = 0
        for e in self.events:
            cols += e.columns
            if e.start == self.start:
                msg += e.render(indent_level)
        if cols == 0:
            msg += insert_indent(indent_level)
            msg += '<td colspan=\"{0}\">&nbsp;</td>'.format(max_conflicts)
        else:
            while cols < max_conflicts:
                msg += insert_indent(indent_level)
                msg += '<td colspan=\"1\" class=\"split\">&nbsp;</td>'
                cols += 1
        return msg

slots = dict()

class Event(object):
    def __init__(self, day, start, length, kind, data):
        self.day = day
        self.start = start
        self.length = length
        self.rows = int(round(length * 2))
        self.columns = max_conflicts
        self.kind = kind
        self.data = data
        for i in range(self.rows):
            global slots
            time = start + 0.5 * i
            key = Slot(day, time)
            if key in slots:
                key = slots[key]
            else:
                slots[key] = key
            key.add_event(self)
    def render(self, indent_level):
        cls = 'slot ' + self.kind
        if self.columns == 1:
            cls += ' split'
        msg = insert_indent(indent_level)
        msg += '<td rowspan=\"{0}\" colspan=\"{1}\" class=\"{2}\">'.format(self.rows, self.columns, cls)
        for datum in self.data:
            msg += insert_indent(indent_level+2)
            msg += datum
            msg += '<br/>'
        msg += insert_indent(indent_level)
        msg += '</td>'
        return msg

mon, tue, wed, thu, fri = 0, 1, 2, 3, 4

lectures = [Event(mon, 14, 1, 'lecture', ['Lecture 001', '1 Pimentel']),
            Event(mon, 16, 1, 'lecture', ['Lecture 002', '10 Evans']),
            Event(wed, 14, 1, 'lecture', ['Lecture 001', '1 Pimentel']),
            Event(wed, 16, 1, 'lecture', ['Lecture 002', '10 Evans']),
            Event(fri, 14, 1, 'lecture', ['Lecture 001', '1 Pimentel']),
            Event(fri, 16, 1, 'lecture', ['Lecture 002', '10 Evans'])]

labs = [Event(tue, 8,    1.5, 'lab', ['Lab 011', '271 Soda', 'Richard Hwang']),
        Event(tue, 9.5,  1.5, 'lab', ['Lab 012', '271 Soda', 'Richard Hwang']),
        Event(tue, 11,   1.5, 'lab', ['Lab 013', '271 Soda', 'Joy Jeng']),
        Event(tue, 12.5, 1.5, 'lab', ['Lab 014', '271 Soda', 'Stephen Martinis']),
        Event(tue, 14,   1.5, 'lab', ['Lab 015', '271 Soda', 'Julia Oh']),
        Event(tue, 15.5, 1.5, 'lab', ['Lab 016', '271 Soda', 'Julia Oh']),
        Event(tue, 17,   1.5, 'lab', ['Lab 017', '271 Soda', 'Robert Huang']),
        Event(tue, 18.5, 1.5, 'lab', ['Lab 018', '271 Soda', 'Robert Huang']),
        Event(tue, 20,   1.5, 'lab', ['Lab 019', '271 Soda', 'Sharad Vikram']),
        Event(tue, 21.5, 1.5, 'lab', ['Lab 020', '271 Soda', 'Sharad Vikram']),
        Event(wed, 8,    1.5, 'lab', ['Lab 021', '271 Soda', 'Soumya Basu']),
        Event(wed, 9.5,  1.5, 'lab', ['Lab 022', '271 Soda', 'Hamilton Nguyen']),
        Event(wed, 11,   1.5, 'lab', ['Lab 023', '271 Soda', 'Mark Miyashita']),
        Event(wed, 12.5, 1.5, 'lab', ['Lab 024', '271 Soda', 'Soumya Basu']),
        Event(wed, 14,   1.5, 'lab', ['Lab 025', '271 Soda', 'Albert Wu']),
        Event(tue, 8,    1.5, 'lab', ['Lab 026', '273 Soda', 'Keegan Mann']),
        Event(tue, 9.5,  1.5, 'lab', ['Lab 027', '273 Soda', 'Mark Miyashita']),
        Event(tue, 21.5, 1.5, 'lab', ['Lab 028', '273 Soda', 'Keegan Mann'])]

discs = [Event(thu, 8,    1.5, 'dis', ['Dis 011', '6 Evans', 'Richard Hwang']),
         Event(thu, 9.5,  1.5, 'dis', ['Dis 012', '6 Evans', 'Richard Hwang']),
         Event(thu, 11,   1.5, 'dis', ['Dis 013', '75 Evans', 'Joy Jeng']),
         Event(thu, 12.5, 1.5, 'dis', ['Dis 014', '6 Evans', 'Stephen Martinis']),
         Event(thu, 14,   1.5, 'dis', ['Dis 015', '4 Evans', 'Julia Oh']),
         Event(thu, 15.5, 1.5, 'dis', ['Dis 016', '75 Evans', 'Julia Oh']),
         Event(thu, 17,   1.5, 'dis', ['Dis 017', '71 Evans', 'Robert Huang']),
         Event(thu, 18.5, 1.5, 'dis', ['Dis 018', 'B56 Hildebrand', 'Robert Huang']),
         Event(thu, 20,   1.5, 'dis', ['Dis 019', '310 Soda', 'Sharad Vikram']),
         Event(thu, 21.5, 1.5, 'dis', ['Dis 020', '310 Soda', 'Sharad Vikram']),
         Event(fri, 8,    1.5, 'dis', ['Dis 021', '310 Soda', 'Soumya Basu']),
         Event(fri, 9.5,  1.5, 'dis', ['Dis 022', '310 Soda', 'Hamilton Nguyen']),
         Event(fri, 11,   1.5, 'dis', ['Dis 023', '310 Soda', 'Mark Miyashita']),
         Event(fri, 12.5, 1.5, 'dis', ['Dis 024', '310 Soda', 'Soumya Basu']),
         Event(fri, 14,   1.5, 'dis', ['Dis 025', '310 Soda', 'Albert Wu']),
         Event(thu, 8,    1.5, 'dis', ['Dis 026', '4 Evans', 'Keegan Mann']),
         Event(thu, 9.5,  1.5, 'dis', ['Dis 027', '3105 Etcheverry', 'Mark Miyashita']),
         Event(thu, 21.5, 1.5, 'dis', ['Dis 028', '320 Soda', 'Keegan Mann'])]

def render_tablehead():
    msg = """  <tr>
    <th></th>
    <th colspan="{0}">Monday</th>
    <th colspan="{0}">Tuesday</th>
    <th colspan="{0}">Wednesday</th>
    <th colspan="{0}">Thursday</th>
    <th colspan="{0}">Friday</th>
  </tr>"""
    return msg.format(max_conflicts)

def insert_indent(n):
    """First does a newline, then indents by n spaces."""
    result = '\n'
    for i in range(n):
        result += ' '
    return result

def render_time(start, indent_level):
    msg = insert_indent(indent_level) + '<tr>'
    indent_level += 2
    msg += insert_indent(indent_level)
    msg += '<td class=\"time\">{0}:{1}</td>'.format(int(start if start < 13 else start - 12), '00' if start == int(start) else '30')
    for i in range(5):
        key = Slot(i, start)
        if key in slots:
            key = slots[key]
        msg += key.render(indent_level)
    indent_level -= 2
    msg += insert_indent(indent_level) + '</tr>'
    return msg

def render_schedule(start=8, end=23):
    output = ['<table class="schedule">']
    output.append(render_tablehead())
    while start < end:
        output.append(render_time(start, 2))
        start += 0.5
    output.append('</table>')
    return output

def update_index_html():
    """The '<!-- BEGIN SCHEDULE -->' and '<!-- END SCHEDULE -->' markers must
    surround the schedule table in index.html.
    """
    path = 'www/index.html'
    old = open(path).readlines()
    revised = []
    copying, inserted = True, False
    start_line, end_line = '<!-- BEGIN SCHEDULE -->', '<!-- END SCHEDULE -->'
    for line in old:
        if line.endswith('\n'): line = line[:-1]
        if line.strip() == start_line:
            print >>sys.stderr, 'Inserting schedule at BEGIN'
            revised.append(line)
            revised.extend(render_schedule())
            copying = False
        elif line.strip() == end_line:
            copying, inserted = True, True
            print >>sys.stderr, 'Found END'
        if copying:
            revised.append(line)
    if inserted:
        print >>sys.stderr, 'Writing revised', path
        file(path, 'w').write('\n'.join(revised))

update_index_html()
