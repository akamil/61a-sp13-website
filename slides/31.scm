; Symbolic programming
(define x 3)

x
'x

(define poly '(+ (* 3 x x) (* 4 x) 1))

poly

(define (eval-poly poly x) (eval poly))

(eval-poly poly 1)
(eval-poly poly 2)
(eval-poly poly 3)

; Let
(define (filter fn s)
  (if (null? s)
      s
      (let ((first (car s))
            (rest (filter fn (cdr s))))
        (if (fn first)
            (cons first rest)
            rest))))

(define (filter-comp comp pivot s)
  (filter (lambda (x) (comp x pivot)) s))

(define (quick-sort s)
  (if (<= (length s) 1)
      s
      (let ((pivot (car s)))
        (append (quick-sort (filter-comp < pivot s))
                (filter-comp = pivot s)
                (quick-sort (filter-comp > pivot s))))))

(quick-sort '(9 3 1 6 5 8 7))

; Begin
(define (repeat k fn)
  (if (> k 0)
      (begin (fn) (repeat (- k 1) fn))
      'done))

(define (tri fn)
  (repeat 3 (lambda () (fn) (lt 120))))

(define (sier d k)
  (tri (lambda ()
         (if (= k 1)
             (fd d)
             (leg d k)))))

(define (leg d k)
  (sier (/ d 2) (- k 1))
  (penup)
  (fd d)
  (pendown))

; This won't work in STk but will in your project 4 solution
; (sier 400 6)

; Language extension; you don't have to know anything that appears here
(define-macro (infix . args)
  (if (= (length args) 1)
      (car args)
      `(infix , (list (cadr args) (car args) (caddr args)) ,@ (cdddr args))))

(infix 3 + 4 * 7 - 1)

(define-macro (when test . branch)
  (list 'if test (cons 'begin branch)))

(when (< x 3)
  (print 1)
  (print 2))

(when (< x 4)
  (print 1)
  (print 2)
  (print 3))
