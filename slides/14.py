from operator import add, mul
from math import sqrt, e

# Rational numbers
def mul_rational(x, y):
    """Multiply two rationals together.

    >>> r = mul_rational(rational(3, 2), rational(3, 5))
    >>> numer(r) / denom(r)
    0.9
    """
    return rational(numer(x) * numer(y), denom(x) * denom(y))

def add_rational(x, y):
    """Add two rationals together.

    >>> r = add_rational(rational(3, 2), rational(3, 5))
    >>> numer(r) / denom(r)
    2.1
    """
    nx, dx = numer(x), denom(x)
    ny, dy = numer(y), denom(y)
    return rational(nx * dy + ny * dx, dx * dy)

def eq_rational(x, y):
    """Determine if two rationals are equal.

    >>> eq_rational(rational(1, 2), rational(2, 4))
    True
    >>> eq_rational(rational(1, 2), rational(3, 4))
    False
    """
    return numer(x) * denom(y) == numer(y) * denom(x)

# Representing rationals using tuples
def rational(n, d):
    """Construct a rational number x that represents n/d."""
    return (n, d)

from operator import getitem

def numer(x):
    """Return the numerator of rational number x."""
    return getitem(x, 0)

def denom(x):
    """Return the denominator of rational number x."""
    return getitem(x, 1)

# Reducing to lowest terms
from fractions import gcd

def rational(n, d):
    """Construct a rational number x that represents n/d."""
    g = gcd(n, d)
    return (n // g, d // g)

# Representing pairs using functions
def pair(x, y):
    """Return a functional pair."""
    def dispatch(m):
        if m == 0:
            return x
        else:
            return y
    return dispatch

def getitem_pair(p, i):
    """Return the element at index i of pair p.

    >>> p = pair(1, 2)
    >>> getitem_pair(p, 0)
    1
    >>> getitem_pair(p, 1)
    2
    """
    return p(i)

# Recursive lists using pairs
empty_rlist = None

def rlist(first, rest):
    """Return a recursive list from its first element and the rest."""
    return (first, rest)

def first(s):
    """Return the first element of a recursive list s."""
    return s[0]

def rest(s):
    """Return the rest of the elements of a recursive list s."""
    return s[1]

counts = rlist(1, rlist(2, rlist(3, rlist(4, empty_rlist))))

# Implementing the sequence abstraction using recursion
def len_rlist(s):
    """Return the length of a recursive list s.
    
    >>> len_rlist(counts)
    4
    """
    if s == empty_rlist:
        return 0
    return 1 + len_rlist(rest(s))

def getitem_rlist(s, i):
    """Return the element at index i of recursive list s.

    >>> getitem_rlist(counts, 2)
    3
    """
    if i == 0:
        return first(s)
    return getitem_rlist(rest(s), i - 1)

# Implementing the sequence abstraction using iteration
def len_rlist_iterative(s):
    """Return the length of a recursive list s.
    
    >>> len_rlist_iterative(counts)
    4
    """
    length = 0
    while s != empty_rlist:
        s, length = rest(s), length + 1
    return length

def getitem_rlist_iterative(s, i):
    """Return the element at index i of recursive list s.

    >>> getitem_rlist_iterative(counts, 2)
    3
    """
    while i > 0:
        s, i = rest(s), i - 1
    return first(s)
