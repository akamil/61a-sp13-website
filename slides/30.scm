; Call expressions
(+ 1 2 3 4)
(+)
(*)
(- 12)
(- 20 1 2 3 4 5)
(number? 12)
(integer? 3.3)
(zero? 2)

; Definitions
(define (square x) (* x x))

(define (average x y) (/ (+ x y) 2))

(define (abs x)
  (if (< x 0)
      (- x)
      x))

(define (sqrt a)
  (define (done? x)
    (< (abs (- (square x) a)) 0.0000001))
  (define (update x)
    (average x (/ a x)))
  (define (iter-improve guess)
    (if (done? guess)
        guess
        (iter-improve (update guess))))
  (iter-improve 1))

; Pairs and lists
(define (length items)
  (if (null? items)
      0
      (+ 1 (length (cdr items)))))

(define (getitem items n)
  (if (= n 0)
      (car items)
      (getitem (cdr items) (- n 1))))

(define squares (list 1 4 9 16 25)) 

(length squares)
(getitem squares 3)

; Symbolic programming
(define x 3)

x
'x

(define poly '(+ (* 3 x x) (* 4 x) 1))

poly

(define (eval-poly poly x) (eval poly))

(eval-poly poly 1)
(eval-poly poly 2)
(eval-poly poly 3)

; Language extension; you don't have to know anything that appears here
(define-macro (infix . args)
  (if (= (length args) 1)
      (car args)
      `(infix , (list (cadr args) (car args) (caddr args)) ,@ (cdddr args))))

(infix 3 + 4 * 7 - 1)

(define-macro (when test . branch)
  (list 'if test (cons 'begin branch)))

(when (< x 3)
  (print 1)
  (print 2))

(when (< x 4)
  (print 1)
  (print 2)
  (print 3))
