from operator import add
from math import pi, sqrt

# Generalizing patterns
def square_area(r):
    return pow(r, 2)

def circle_area(r):
    return pow(r, 2)

def hexagon_area(r):
    return 3 * sqrt(3) / 2 * pow(r, 2)

def shape_area(r, shape_constant):
    assert r >= 0, "r must not be negative"
    return shape_constant * pow(r, 2)

def square_area(r):
    return shape_area(r, 1)

# Generalizing over computational processes
def identity(k):
    return k

def sum_integers(n):
    """Return the sum of the first n positive integers"""
    total, k = 0, 1
    while k <= n:
        total, k = total + k, k + 1
    return total

def cube(k):
    return pow(k, 3)

def sum_cubes(n):
    """Return the sum of the first n positive integers"""
    total, k = 0, 1
    while k <= n:
        total, k = total + cube(k), k + 1
    return total

def summation(n, term):
    """Return the sum of the first n terms of a sequence.
    
    >>> summation(5, cube)
    225
    """
    total, k = 0, 1
    while k <= n:
        total, k = total + term(k), k + 1
    return total

def sum_cubes(n):
    return summation(n, cube)

# Functions as return values
def make_adder(n):
    """Return a function that adds n to its argument.

    >>> add_three = make_adder(3)
    >>> add_three(4)
    7
    """
    def adder(k):
        return add(n, k)
    return adder

add_three = make_adder(3)
result = add_three(4)
make_adder(1)(2)
