from BeautifulSoup import BeautifulSoup
from urllib2 import urlopen
import re

"""
Some utility functions/classes in order to automatically take in the
table of contents from each chapter of the textbook, and turn it
into a form so that schedule.py can easily automatically generate
Readings links.

Major thanks goes to the BeautifulSoup library:
    http://www.crummy.com/software/BeautifulSoup/

    - Eric Kim, Fall 2011
"""

def generate_subsections(chapter_url):
    """
    Returns a mapping from section number to section name, like:
      { '1.1.4' : 'first-example' }
    """
    print('Loading ' + chapter_url)
    html = ''.join(urlopen(chapter_url))
    soup = BeautifulSoup(html)
    stuff = soup.findAll('a', href=re.compile(r'#\a*'))
    mapping = {}
    for x in stuff:
        if x['href'][1:3] != 'id':
            pattern = r'((?:\d\.)*\d)'
            m = re.match(pattern, x.string)
            if m:
                mapping[m.group(0)] = x['href'][1:]
    return mapping

class Textbook:
    """
    Textbook is a class that turns the table of contents in every
    chapter listed in self.chapter_urls into a canonical form.

    If you want to add new chapters to Textbook, add them to
    self.chapter_urls.
    """

    base_url = 'http://inst.eecs.berkeley.edu/~cs61a/book/chapters/'
    chapter_urls = { '1' : base_url + 'functions.html',
                     '2' : base_url + 'objects.html',
                     '3' : base_url + 'interpretation.html',
                     '4' : base_url + 'streams.html',
                     #'5' : base_url + 'streams.html',
                      # More chapters go here
                   }

    chapter_stubs = None

    def __init__(self):
        self.chapter_stubs = {}
        for chapter in self.chapter_urls:
            mapping = generate_subsections(self.chapter_urls[chapter])
            self.chapter_stubs[chapter] = mapping

    def chapter_num(self, section):
        return section[0:section.find('.')]

    def get_subsection_stub(self, section):
        """
        Given a section, return the 'stub',
        which, in the following URL, is the stuff after the '#':
          http://wla.berkeley.edu/~cs61a/fa11/lectures/functions.html#first-example
        """
        return self.chapter_stubs[self.chapter_num(section)][section]

    def get_url(self, section):
        """
        Given a section number (like 1.1.4), return a pair, whose
        first element is the section number (1.1.4) and whose second
        element is the link, like:
          http://wla.berkeley.edu/~cs61a/fa11/lectures/functions.html#first-example

        returns None for the second element if the section is not found.
        """
        chapter = self.chapter_num(section)
        chapter_url = self.chapter_urls.get(chapter, None)
        if chapter_url:
            subsection_stub = self.get_subsection_stub(section)
            return (section, chapter_url + '#' + subsection_stub)
        else:
            return (section, None)

def test_Textbook():
    print "==== Testing Textbook class."
    textbook = Textbook()
    assert textbook.get_url('1.1.4') == 'http://wla.berkeley.edu/~cs61a/fa11/lectures/functions.html#first-example', 'was instead: {0}'.format(textbook.get_url('1.1.4'))
    assert textbook.get_url('1.3.1') == 'http://wla.berkeley.edu/~cs61a/fa11/lectures/functions.html#environments', 'was instead: {0}'.format(textbook.get_url('1.3.1'))
    assert textbook.get_url('1.6.8') == 'http://wla.berkeley.edu/~cs61a/fa11/lectures/functions.html#function-decorators', 'was instead: {0}'.format(textbook.get_url('1.6.8'))
    assert textbook.get_url('1.5') == 'http://wla.berkeley.edu/~cs61a/fa11/lectures/functions.html#control', 'was instead: {0}'.format(textbook.get_url('1.5'))
    print "==== Finished Textbook tests, all tests successful."

if __name__ == '__main__':
    test_Textbook()
