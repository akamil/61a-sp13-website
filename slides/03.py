# Import
from operator import add, sub, mul, truediv, floordiv
5 / 4
5 // 4
8 / 4
8 // 4
truediv(8, 4)
floordiv(8, 4)
add(14, 28)
add(mul(5, 5), mul(12, 12))

from math import sqrt
sqrt(169)

# Assignment
from math import pi
pi * 71 / 223

radius = 10
2 * radius
area, circ = pi * radius * radius, 2 * pi * radius
radius = 5
area, circ = pi * radius * radius, 2 * pi * radius

# Function values
max
f = max
f(3, 4)
f = 2
# f(3, 4)

# Definitions
from operator import add, mul
def square(x):
    return mul(x, x)
square(21)
square(add(2, 5))
square(square(3))

def sum_squares(x, y):
    return add(square(x), square(y))
sum_squares(3, 4)
sum_squares(5, 12)

# Name conflicts
# def square(square):
#     return mul(square, square)
# square(4)
# def square(mul):
#     return mul(mul, mul)
# square(4)

# Multiple return values
def divide_exact(n, d):
    return n // d, n % d
quotient, remainder = divide_exact(2012, 10)
