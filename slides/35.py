# Iteration vs. recursion
def factorial_iter(n):
    """Compute n!

    >>> factorial_iter(6)
    720
    """
    if n == 0:
         return 1
    return n * factorial(n - 1)

def factorial_rec(n):
    """Compute n!

    >>> factorial_rec(6)
    720
    """
    total = 1
    while n > 0:
        n, total = n - 1, total * n
    return total

# Iteration using recursive syntax
def factorial(n, total=1):
    """Compute n!

    >>> factorial(6)
    720
    """
    if n == 0:
        return total
    return factorial(n - 1, total * n)
