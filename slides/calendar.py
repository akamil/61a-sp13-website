from scrape_textbook import Textbook
from datetime import date, datetime, timedelta
import os
import sys

"""
Note: Because scrape_textbook.py uses the BeautifulSoup library, which
doesn't yet run correctly in Python 3, this script should be run
using Python 2.

Sorry about the inconvenience.
    - Eric Kim
"""

textbook = Textbook()

lectures = ['Functions',
            'Values and Side Effects',
            'Names',
            'Control and Higher-Order Functions',
            'Higher-Order Functions and ***In-Class Quiz***',
            'Environments and Lambda',
            'Currying and Newton\'s Method',
            'Recursion',
            'Tree Recursion',
            'Data Abstraction',
            'Sequences',
            'Objects and Strings',
            'Lists and Dictionaries',
            'Mutable Data',
            'Mutable Data Types',
            'Object-Oriented Programming',
            'Inheritance',
            'Multiple Representations',
            'Generic Functions',
            'Recursive Data',
            'Orders of Growth',
            'Sets',
            'User Interfaces',
            'Functional Programming',
            'Exceptions',
            'Calculator',
            'Interpreters',
            'Tail Calls',
            'The Halting Problem',
            'Iterators',
            'Streams',
            'Declarative Programming',
            'Unification',
            'Distributed Computing',
            'MapReduce',
            'Parallelism',
            'Conclusion',
            'Special Lecture'
            ]

readings = {date(2013, 1, 23) : (textbook.get_url('1.1'),
                                 textbook.get_url('1.2')),
            date(2013, 1, 25) : (textbook.get_url('1.3'),
                                 textbook.get_url('1.4')),
            date(2013, 1, 28) : (textbook.get_url('1.5'),),
            date(2013, 1, 30)  : (textbook.get_url('1.6'),),
            date(2013, 2, 1)  : (textbook.get_url('1.6'),),
            date(2013, 2, 4)  : (textbook.get_url('1.6'),),
            date(2013, 2, 6)  : (textbook.get_url('1.6'),),
            date(2013, 2, 8)  : (textbook.get_url('1.7'),),
            date(2013, 2, 11)  : (textbook.get_url('1.7'),),
            date(2013, 2, 15)  : (textbook.get_url('2.2'),),
            date(2013, 2, 20)  : (textbook.get_url('2.3'),),
            date(2013, 2, 22)  : (textbook.get_url('2.1'),),
            date(2013, 2, 25)  : (textbook.get_url('2.4'),),
            date(2013, 2, 27)  : (textbook.get_url('2.4'),),
            date(2013, 3, 1)  : (textbook.get_url('2.4'),),
            date(2013, 3, 4)  : (textbook.get_url('2.5'),),
            date(2013, 3, 6)  : (textbook.get_url('2.6'),),
            date(2013, 3, 8)  : (textbook.get_url('2.7'),),
            date(2013, 3, 11)  : (textbook.get_url('2.7'),),
            date(2013, 3, 13)  : (textbook.get_url('3.3'),),
            date(2013, 3, 15)  : (textbook.get_url('3.2'),),
            date(2013, 3, 18)  : (textbook.get_url('3.3'),),
            date(2013, 4, 1)  : (textbook.get_url('3.5'),),
            date(2013, 4, 3)  : (textbook.get_url('3.4'),),
            date(2013, 4, 5)  : (textbook.get_url('3.6'),),
            date(2013, 4, 8)  : (textbook.get_url('3.7'),),
            date(2013, 4, 10)  : (textbook.get_url('3.7'),),
            date(2013, 4, 15)  : (textbook.get_url('4.2'),),
            date(2013, 4, 17)  : (textbook.get_url('4.2'),),
            date(2013, 4, 19)  : (textbook.get_url('4.3'),),
            date(2013, 4, 22)  : (textbook.get_url('4.4'),),
            date(2013, 4, 24)  : (textbook.get_url('4.5'),),
            date(2013, 4, 26)  : (textbook.get_url('4.6'),),
           }

labs = {date(2013, 1, 21): (('Lab 0: Intro to Unix/Emacs', 'labs/lab00/lab00.php'),),
         date(2013, 1, 28): (('Lab 1: Control Flow', 'labs/lab01/lab01.php'),),
#        date(2013, 2, 4): (('Lab 2: Higher-Order Functions', 'labs/lab02/lab02.php'),),
#        date(2013, 2, 11): (('Lab 3: Recursion', 'labs/lab03/lab03.php'),),
#        date(2013, 2, 18): (('Lab 4: Data Abstraction', 'labs/lab04/lab04.html'),),
#        date(2013, 2, 25): (('Lab 5: Guitar Heroine', 'labs/lab05/lab5.html'),),
#        date(2013, 3, 4): (('Lab 6: Shakespeare', 'labs/lab06/lab06.php'),),
#        date(2013, 3, 11): (('Lab 7: Object-Oriented Programming', 'labs/lab07/lab7.html'),),
#        date(2013, 3, 18): (('Lab 8: Recursive Data and Sets', 'labs/lab08/lab08.php'),),
#        date(2013, 4, 1): (('Lab 9: Scheme', 'labs/lab09/lab09.php'),),
#        date(2013, 4, 8): (('Lab 10: Regular Expressions and Calculator', 'labs/lab10/lab10.php'),),
#        date(2013, 4, 15): (('Lab 11: Iterators', 'labs/lab11/lab11.html'),),
#        date(2013, 4, 22): (('Lab 12: Logic Programming', 'labs/lab12/lab12.php'),),
#        date(2013, 4, 29): (('Lab 13: MapReduce', 'labs/lab13/lab13.php'),),
  }

discussions = {date(2013, 1, 23): (
        ('Discussion 1: Unix Review, Expressions, and Functions', 'disc/disc01.pdf'),
        ('Solutions', 'disc/disc01_sol.pdf'),
        ),
    date(2013, 1, 30): (
        ('Discussion 2: Higher-Order Functions', 'disc/disc02.pdf'),
#        ('Solutions', 'disc/disc02_sol.pdf'),
        ),
#    date(2013, 2, 6): (
#        ('Discussion 3: Newton\'s Method and Sequences', 'disc/disc03.pdf'),
#        ('Solutions', 'disc/disc03_sol.pdf'),
#        ),
#    date(2013, 2, 13): (
#        ('Discussion 4: Mutable Lists and Dictionaries', 'disc/disc04.pdf'),
#        ('Solutions', 'disc/disc04_sol.pdf'),
#        ),
#    date(2013, 2, 20): (
#        ('Discussion 5: Mutation and Nonlocal Statements', 'disc/disc05.pdf'),
#        ('Solutions', 'disc/disc05_sol.pdf'),
#        ),
#    date(2013, 2, 27): (
#        ('Discussion 6: Object-Oriented Programming', 'disc/disc06.pdf'),
#        ('Solutions', 'disc/disc06_sol.pdf'),
#        ),
#    date(2013, 3, 6): (
#        ('Discussion 7: Recursive Functions', 'disc/disc07.pdf'),
#        ('Solutions', 'disc/disc07_sol.pdf'),
#        ),
#    date(2013, 3, 13): (
#        ('Discussion 8: Rlists, Trees, and Orders of Growth', 'disc/disc08.pdf'),
#        ('Solutions', 'disc/disc08_sol.pdf'),
#        ),
#    date(2013, 3, 20): (
#        ('Discussion 9: Scheme', 'disc/disc09.pdf'),
#        ('Solutions', 'disc/disc09_sol.pdf'),
#        ),
#    date(2013, 4, 3): (
#        ('Discussion 10: Exceptions and Calculator', 'disc/disc10.pdf'),
#        ('Solutions', 'disc/disc10_sol.pdf'),
#        ),
#    date(2013, 4, 10): (
#        ('Discussion 11: Tail Recursion and Dynamic Scope', 'disc/disc11.pdf'),
#        ('Solutions', 'disc/disc11_sol.pdf'),
#        ),
#    date(2013, 4, 17): (
#        ('Discussion 12: Iterators, Generators, and Streams', 'disc/disc12.pdf'),
#        ('Solutions', 'disc/disc12_sol.pdf'),
#        ),
#    date(2013, 4, 24): (
#        ('Discussion 13: Logic Programming', 'disc/disc13.pdf'),
#        ('Solutions', 'disc/disc13_sol.pdf'),
#        ),
#    date(2013, 5, 1): (
#        ('Discussion 14: Parallelism', 'disc/disc14.pdf'),
#        ('Solutions', 'disc/disc14_sol.pdf'),
#        ),
    }

holidays = {date(2013, 1, 21): 'Academic Holiday',
            date(2013, 2, 18): 'Academic Holiday',
            date(2013, 3, 25): 'Academic Holiday',
            date(2013, 3, 27): 'Academic Holiday',
            date(2013, 3, 29): 'Academic Holiday' }

midterms = [date(2013, 2, 13), date(2013, 3, 21)]

projects = {date(2013, 1, 28): ('Hog', datetime(2013, 2, 11, 23, 59), True),
            date(2013, 2, 22): ('Trends', datetime(2013, 3, 5, 23, 59), False),
            date(2013, 3, 4): ('Ants', datetime(2013, 3, 18, 23, 59), False),
            date(2013, 4, 1): ('Hog revisions', datetime(2013, 4, 8, 23, 59), False),
            date(2013, 4, 8): ('Scheme', datetime(2013, 4, 29, 23, 59), False)
            }

# Homeworks have an index, which generates files and name.
hws = {
        date(2013, 1, 23): 0,
        date(2013, 1, 25): 1,
        date(2013, 1, 30): 2,
        date(2013, 2, 6): 3,
        date(2013, 2, 13): 4,
        date(2013, 2, 20): 5,
        date(2013, 2, 27): 6,
        date(2013, 3, 6): 7,
        date(2013, 3, 13): 8,
        date(2013, 3, 20): 9,
        date(2013, 4, 3): 10,
        date(2013, 4, 10): 11,
        date(2013, 4, 17): 12,
        date(2013, 4, 24): 13,
        date(2013, 5, 1): 14
        }

def mwf(d=date(2013, 1, 21), end=date(2013, 2, 11)):
    while d < end:
        yield d
        d += timedelta(days=(3 if d.weekday() == 4 else 2))

def next_event(d, l = list(reversed(lectures))):
    """Yield a lecture title string and a lecture number"""
    if d in holidays:
        return '***' + holidays[d] + '***', 0
    elif d in midterms:                 # Midterm the day of lecture
        return '***No Lecture; Midterm is %s @ 7pm***' % d.strftime("%A %m/%d"), 0
    elif (d-timedelta(1)) in midterms:  # Midterm the day before lecture
        d = d - timedelta(1)
        return '***No Lecture; Midterm is %s @ 7pm***' % d.strftime("%A %m/%d"), 0
    elif len(l) == 0:
        return '???', 0
    else:
        return l.pop(), len(lectures) - len(l)

schedule = [(d, next_event(d)) for d in mwf()]

def make_link(url, text):
    """Create a new <a href=...> ... </a>."""
    return "<a href=\""+url+"\">"+text+"</a>"

def cell(s, options=None):
    """Creates a table cell <td>, with optional options, which is a dictionary
    mapping keys to values, such as:
      {'rowspan' : 3}
    """
    if not options:
        return '<td> ' + s + ' </td>'
    else:
        result = '<td'
        for option in options:
            result += ' ' + option + '=' + str(options[option])
        result = result + '> ' + s + ' </td>'
        return result

def make_p(text, options=None):
    """Create a paragraph tag <p>...</p> with optional options, which is a
    dictionary mapping keys to values, such as:
      {'class' : 'code'}
    """
    if not options:
        return '<p> ' + text + '</p>'
    else:
        result = '<p'
        for option in options:
            result += ' ' + option + '=' + str(options[option])
        result = result + '> ' + s + ' </p>'
        return result

def render_tablehead():
    msg = """  <thead>
    <tr>
      <th>Week</th>
      <th>Date</th>
      <th>Lecture Topic</th>
      <th>Reading</th>
      <th>Lab/Discussion</th>
      <th>Homework</th>
      <th>Project</th>
    </tr>
  </thead>"""
    return msg

def isnewweek(date, week_num):
    return date.weekday() == 0 or week_num == 1

def render_new_week(week_num):
    rowspan_val = 3 #1 if week_num == 1 else 3
    return cell(str(week_num), {'rowspan':rowspan_val, 'class':"weeknum"})

def insert_indent(n):
    """First does a newline, then indents by n spaces."""
    result = '\n'
    for i in range(n):
        result += ' '
    return result

def render_project(name, due, islive):
    project_name = name + ' project'
    if islive:
        fmt = '<a href="projects/{n}/{n}.html">{pn}</a>'
        project_name = fmt.format(n=name.lower(), pn=project_name)
    return '{0} (due {1})'.format(project_name, due.strftime('%b %d at %I:%M%p'))

def render_midterm(date):
    return 'Midterm {0}'.format(midterms.index(date) + 1)

def render_readings(date):
    msg = ''
    if date in readings:
        lst = readings[date]
        stuff = ''
        for reading in lst:
            text = 'Chapter ' + reading[0]
            if reading[1]:
                text = make_link(reading[1], text)
            stuff += make_p(text + '<br/>')
        msg += cell(stuff)
    else:
        msg += cell('')
    return msg

def render_labs_disc(date):
    msg = ''
    lab_lst = labs[date] if date in labs else None
    disc_lst = discussions[date] if date in discussions else None
    # lab, disc are tuples of form:
    #   ( <lab/disc name> , <link to lab/disc> )
    if lab_lst:
        for lab in lab_lst:
            msg += make_p(make_link(lab[1], lab[0]))
        msg += '<br/>' if disc_lst else ''
    if disc_lst:
        for disc in disc_lst:
            msg += make_p(make_link(disc[1], disc[0]))
    return cell(msg)

def render_hw(idx, date, current_path='www/'):
    p = 'hw{0}.'.format(idx)
    files = [f for f in os.listdir(current_path + '/hw') if f.startswith(p)]
    s = ''
    for f in files:
        s += make_link('hw/' + f, f) + ' '
    return s

def render_lecture(name, number, current_path='www/'):
    """Slides format example: 61A-001-Intro_6PP.pdf"""
    prefix = '%02d' % number
    slides = [s for s in os.listdir(current_path + 'slides/') if s.startswith(prefix)]
    slides.sort() # make sure XX.anything comes after XX-anything

    def fmt(s):
        """Grab the 6PP from the slide file name"""
        if s.endswith('.pdf'):
            return s.split('_')[-1].split('.')[0]
        return s

    links = ['(' + make_link('slides/' + s, fmt(s)) + ')' for s in slides]
    return name + ' ' + ' '.join(links)

def render(schedule, projects, midterms, current_path='www/'):
    output = ['<table class="calendar">']
    output.append(render_tablehead())
    week_num = 1
    for d, lecture_tuple in schedule:
        lecture, lecture_number = lecture_tuple
        msg = '  <tr>'
        indent_level = 4
        if isnewweek(d, week_num):
          msg += insert_indent(indent_level)
          msg += render_new_week(week_num)
          week_num += 1
        msg += insert_indent(indent_level)
        msg += cell(d.strftime('%a %b %d'), {'class':"date"})
        msg += insert_indent(indent_level)
        msg += cell(render_lecture(lecture, lecture_number, current_path))
        msg += insert_indent(indent_level)

        msg += render_readings(d)

        msg += insert_indent(indent_level)

        msg += render_labs_disc(d)

        msg += insert_indent(indent_level)
        msg += cell(render_hw(hws[d], d, current_path) if d in hws else '')
        msg += insert_indent(indent_level)
        msg += cell(render_project(*projects[d]) if d in projects else '')
        indent_level -= 2
        msg += insert_indent(indent_level)
        msg += '</tr>'
        output.append(msg)
    output.append('</table>')
    return output

def update_index_html(current_path='www/'):
    """The '<!-- BEGIN CALENDAR -->' and '<!-- END CALENDAR -->' markers must
    surround the calendar table in index.html.
    """
    path = current_path + 'index.html'
    old = open(path).readlines()
    revised = []
    copying, inserted = True, False
    start_line, end_line = '<!-- BEGIN CALENDAR -->', '<!-- END CALENDAR -->'
    for line in old:
        if line.endswith('\n'): line = line[:-1]
        if line.strip() == start_line:
            print >>sys.stderr, 'Inserting calendar at BEGIN'
            revised.append(line)
            revised.extend(render(schedule, projects, midterms, current_path))
            copying = False
        elif line.strip() == end_line:
            copying, inserted = True, True
            print >>sys.stderr, 'Found END'
        if copying:
            revised.append(line)
    if inserted:
        print >>sys.stderr, 'Writing revised', path
        file(path, 'w').write('\n'.join(revised))

def setup_args():
    """Reads in the command-line argument to determine parent directory 
    of target index.html -- alternatively, the script can be run with no
    arguments when you can make a symlink "www" to the location of 
    index.html.
    """
    import argparse
    description = "Run the script with regards to a relative location of the webite repo."

    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--path', '-p', 
            help='specify a path to index.html')
    args = parser.parse_args()
    
    path = 'www'
    if args.path:
        path = args.path 
    update_index_html(path + '/')
        
setup_args()
