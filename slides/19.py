# Implementing dictionaries
def dictionary():
    """Return a functional implementation of a dictionary.

    >>> d = dictionary()
    >>> d('setitem', 4, 8)
    >>> d('setitem', 'hello', 'world')
    >>> d('getitem', 4)
    8
    >>> d('getitem', 'hello')
    'world'
    >>> d('keys')
    (4, 'hello')
    >>> d('values')
    (8, 'world')
    """
    records = []

    def getitem(key):
        for k, v in records:
            if k == key:
                return v

    def setitem(key, value):
        for item in records:
            if item[0] == key:
                item[1] = value
                return
        records.append([key, value])

    def dispatch(message, key=None, value=None):
        if message == 'getitem':
            return getitem(key)
        elif message == 'setitem':
            setitem(key, value)
        elif message == 'keys':
            return tuple(k for k, _ in records)
        elif message == 'values':
            return tuple(v for _, v in records)

    return dispatch

# Dispatch dictionaries
def account(balance):
    """Return an account that is represented by a dispatch dictionary.

    >>> acc = account(100)
    >>> acc['deposit'](100)
    200
    >>> acc['withdraw'](125)
    75
    >>> acc['balance']
    75
    """
    def withdraw(amount):
        if amount > dispatch['balance']:
            return 'Insufficient funds'
        dispatch['balance'] -= amount
        return dispatch['balance']
    def deposit(amount):
        dispatch['balance'] += amount
        return dispatch['balance']
    dispatch = {'balance': balance, 'withdraw': withdraw,
                'deposit': deposit}
    return dispatch

# OOP
class Account(object):
    """A bank account has a balance and an account holder.

    >>> a = Account('John')
    >>> a.deposit(100)
    100
    >>> a.withdraw(90)
    10
    >>> a.withdraw(90)
    'Insufficient funds'
    >>> a.balance
    10
    """

    interest = 0.02  # A class attribute (unused below)

    def __init__(self, account_holder):
        self.balance = 0
        self.holder = account_holder

    def deposit(self, amount):
        """Add amount to balance."""
        self.balance = self.balance + amount
        return self.balance

    def withdraw(self, amount):
        """Subtract amount from balance if funds are available."""
        if amount > self.balance:
            return 'Insufficient funds'
        self.balance = self.balance - amount
        return self.balance

# Currying to get a bound method
from operator import add

def curry(f):
    def outer(x):
        def inner(*args):
            return f(x, *args)
        return inner
    return outer

add2 = curry(add)(2)
add2(3)

tom_account = Account('Tom')
tom_deposit = curry(Account.deposit)(tom_account)
tom_deposit(1000)
