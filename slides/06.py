def square(x):
    return x * x

# Functions as return values
def make_adder(n):
    """Return a function that adds n to its argument.

    >>> add_three = make_adder(3)
    >>> add_three(4)
    7
    """
    def adder(k):
        return add(n, k)
    return adder

add_three = make_adder(3)
result = add_three(4)
make_adder(1)(2)

# Non-nested environment
def f(x, y):
    return g(x)

def g(a):
    return a + y

#result = f(1, 2)

# Function composition
def compose1(f, g):
    def h(x):
        return f(g(x))
    return h

compose1(square, make_adder(2))(3)

# Lambda expressions
square = lambda x: x * x

def summation(n, term):
    """Return the sum of the first n terms of a sequence.
    
    >>> summation(5, lambda x: pow(x, 3))
    225
    """
    total, k = 0, 1
    while k <= n:
        total, k = total + term(k), k + 1
    return total

result = summation(5, lambda x: x * x)
