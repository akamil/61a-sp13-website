# calculator
2013
2000 + 13
2 * (3 - 4 * 5) + 6

# functions
max(3.2, 8.3)
max(12, 31, 9, 72)
pow(10, 2)
pow(2, 10)

# string manipulation (a string is an arbitrary sequence of characters)
fortyniners = "Super Bowl Champs!"
fortyniners.split()
fortyniners.split()[0]

# open shakespeare's collected works (from local file)
shakespeare = open('shakespeare.txt')

# split into individual words
text = shakespeare.read().decode().split()

# first ten words
text[:10]

# number of words
len(text)

# number of words that are at most 3 letters
len([w for w in text if len(w) <= 3])

# Why is a comma included? Because computer = powerful + stupid; we haven't told the machine to filter out punctuation.

# set of unique words
words = set(text)

# 10 largest words (he really liked hyphens!)
sorted(text, key=len, reverse=True)[:10]

# 10 largest non-hyphenated words (can you say "honorificabilitudinitatibus" ten times fast?)
[w for w in sorted(words, key = len, reverse = True) if '-' not in w][:10]

# how many times did he use it? (only once; maybe his actors rebelled)
text.count("honorificabilitudinitatibus")

# palindromes (excuse the weird notation)
{w for w in text if len(2) > 1 and w == w[::-1]}

# words of at least six letters whose reverse also occurs in shakespeare
{w for w in words if len(w) > 5 and w[::-1] in words}