; Iterative/tail recursive factorial
(define (factorial n total)
  (if (= n 0) total
      (factorial (- n 1)
                 (* total n))))

; Recursive and iterative length
(define (length s)
  (if (null? s) 0
      (+ 1 (length (cdr s)))))

(define (length-tail s)
  (define (length-iter s n)
    (if (null? s) n
        (length-iter (cdr s)
                     (+ 1 n))))
  (length-iter s 0))

; Iterative reduce
(define (reduce procedure s start)
  (if (null? s) start
      (reduce procedure
              (cdr s)
              (procedure start (car s)))))

; Iterative reverse
(define (reverse s)
  (define (reverse-iter s r)
    (if (null? s) r
        (reverse-iter (cdr s)
                      (cons (car s) r))))
  (reverse-iter s nil))

; Recursive and iterative map
(define (map-rec procedure s)
  (if (null? s) s
      (cons (procedure (car s))
            (map-rec procedure (cdr s)))))

(define (map procedure s)
  (define (map-iter procedure s m)
    (if (null? s) m
        (map-iter procedure
                  (cdr s)
                  (cons (procedure (car s)) m))))
  (reverse (map-iter procedure s nil)))

; Tests
(define (assert-equal v1 v2)
  (if (equal? v1 v2)
      (print 'ok)
      (print (list v2 'does 'not 'equal v1))))

(define square (lambda (x) (* x x)))

(assert-equal 360 (factorial 5 3))
(assert-equal 4 (length '(5 6 7 8)))
(assert-equal 4 (length-tail '(5 6 7 8)))
(assert-equal 1680 (reduce * '(5 6 7 8) 1))
(assert-equal '(5 4 3 2)
              (reduce (lambda (x y) (cons y x)) '(3 4 5) '(2)))
(assert-equal '(8 7 6 5) (reverse '(5 6 7 8)))
(assert-equal '(25 36 49 64) (map-rec square '(5 6 7 8)))
(assert-equal '(25 36 49 64) (map square '(5 6 7 8)))
